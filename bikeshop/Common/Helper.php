<?php

class Common_Helper
{

    //转换字符串为0

    private function conversion($string)
    {
        $i = substr($string, 3);

        if ($i == '十') {
            return [
                'price1' => substr($string, 0, 1) . '0',
                'price2' => substr($string, 2, 1) . '0',
            ];
        }

        if ($i == '百') {
            return [
                'price1' => substr($string, 0, 1) . '00',
                'price2' => substr($string, 2, 1) . '00',
            ];
        }

        if ($i == '千') {
            return [
                'price1' => substr($string, 0, 1) . '000',
                'price2' => substr($string, 2, 1) . '000',
            ];
        }

        if ($i == '万') {
            return [
                'price1' => substr($string, 0, 1) . '0000',
                'price2' => substr($string, 2, 1) . '0000',
            ];
        }
    }

    //计算距离
    public function distance($longitude, $latitude)
    {
        $shop = new Domain_HomePageShop();
        $apiShop = new Api_Shop();
        $dateList = $apiShop->getGidShopList();

        foreach ($dateList as $k => $v) {
            //读取数据库中的经纬度

            $distance[] = $shop->distance($longitude, $latitude, $v['longitude'], $v['latitude']);

            //循环根据用户的经纬度
            $reslut[] = $distance[$k][0]["st_distance (point (120.114001,30.288335),point(" . $v['longitude'] . "," . $v['latitude'] . ") ) * 111195"];

            $data[] = round($reslut[$k] / 1000, 2) . '千米';//算出距离返回
        }

        return $data;
    }

    //删除数组中相同元素，只保留一个相同元素
    public function formatArray($array)
    {
        sort($array);
        $tem = "";
        $temarray = array();
        $j = 0;
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] != $tem) {
                $temarray[$j] = $array[$i];
                $j++;
            }
            $tem = $array[$i];
        }
        return $temarray;
    }


    //如果两个数组某个字段相同,就合并成一个数组
    public function formatArr2($arr, $parameter)
    {
        $result = [];
        foreach ($arr as $k => $v) {
            $result[$v[$parameter]][] = $v;
        }

        return array_values($result);
    }


    //判断两个数组是否相同
    public function checkArr($key1, $key2)
    {

        $a = array_diff($key1, $key2);
        $b = array_diff($key2, $key1);
        if (empty($a) && empty($b)) {
            return true;
        } else {
            return false;
        }
    }

    //一维或者二维数组中重复的数据
    public function unique_arr($array2D, $stkeep = false, $ndformat = true)
    {
        // 判断是否保留一级数组键 (一级数组键可以为非数字)
        if ($stkeep) $stArr = array_keys($array2D);
        // 判断是否保留二级数组键 (所有二级数组键必须相同)
        if ($ndformat) $ndArr = array_keys(end($array2D));
        //降维,也可以用implode,将一维数组转换为用逗号连接的字符串
        foreach ($array2D as $v) {
            $v = join(",", $v);
            $temp[] = $v;
        }
        //去掉重复的字符串,也就是重复的一维数组
        $temp = array_unique($temp);
        //再将拆开的数组重新组装
        foreach ($temp as $k => $v) {
            if ($stkeep) $k = $stArr[$k];
            if ($ndformat) {
                $tempArr = explode(",", $v);
                foreach ($tempArr as $ndkey => $ndval) $output[$k][$ndArr[$ndkey]] = $ndval;
            } else $output[$k] = explode(",", $v);
        }
        return $output;
    }

    //去除二维数组的重复项方法
    public function array_unique_fb($array2D)
    {
        foreach ($array2D as $v) {
            $v = join(',', $v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串
            $temp[] = $v;
        }
        $temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组
        foreach ($temp as $k => $v) {
            $temp[$k] = explode(',', $v); //再将拆开的数组重新组装
        }
        return $temp;
    }

    /**
     * 删除数组制定元素,并重新排列
     * @param 要删除的数字 int $arr
     * @param 原有数组 array $arr2
     * @return array 返回值重新排列后的数据 $arr2 array
     */
    public function unsetArr($arr, $arr2)
    {
//        $arr = array(1,3, 5,7,8);
        foreach ($arr2 as $key => $value) {                 //3
            if ($value === $arr)
                unset($arr2[$key]);
        }

        return array_values($arr2);
    }

    //拼接腾讯云图片上传路径
    public function getFileName($directory, $file)
    {
        $suffix = substr($file['type'], 6);

        return '/' . $directory . '/' . date('YmdHis') . uniqid() . '.' . $suffix;
    }

    //二维数组,根据某个字段排序
    public function sort($productData, $arg)
    {
        $sort = array(
            'SORT_DESC' => 'SORT_DESC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
            'SORT_ASC' => 'SORT_ASC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
            'field' => 'retail_price',       //排序字段
        );
        $arrSort = array();
        foreach ($productData AS $uniqid => $row) {
            foreach ($row AS $key => $value) {
                $arrSort[$key][$uniqid] = $value;
            }
        }
        if ($arg == 'ASC') {
            array_multisort($arrSort[$sort['field']], constant($sort['SORT_ASC']), $productData);
        }

        if ($arg == 'DESC') {
            array_multisort($arrSort[$sort['field']], constant($sort['SORT_DESC']), $productData);
        }

        return $productData;

    }

    //门店上架商品筛选
    public function shopDataScreening($count, $productData, $arg, $arg2 = 0, $arg3 = 0)
    {
        if ($count == 2) {
            if ($arg == 'DESC' || $arg == 'ASC') {
                return $this->sort($productData, $arg);

            } else {
                foreach ($productData as $k => $v) {
                    if (in_array($arg, $v)) {
                        $firstData[] = $v;
                    }
                }
                return $firstData;
            }

        }

        if ($count == 3) {
            foreach ($productData as $k => $v) {
                if (in_array($arg, $v)) {
                    $firstData[] = $v;
                }
            }
            if ($arg2 == 'DESC' || $arg2 == 'ASC') {
                return $this->sort($firstData, $arg2);

            } else {
                foreach ($firstData as $k2 => $v2) {
                    if (in_array($arg2, $v2)) {
                        $secondData[] = $v2;
                    }
                }
                return $secondData;

            }

        }

        if ($count == 4) {
            foreach ($productData as $k => $v) {
                if (in_array($arg, $v)) {
                    $firstData[] = $v;
                }
            }
            foreach ($firstData as $k2 => $v2) {
                if (in_array($arg2, $v2)) {
                    $secondData[] = $v2;
                }
            }

            return $this->sort($secondData, $arg3);
        }
    }
  //订单号生成
    public function getOrder($oid=1){

       if($oid==1){ 
         $danhao = 'pp'.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
          return $danhao;
       }else{
         $yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
         $orderSn = $yCode[intval(date('Y')) - 2011] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
         return $orderSn;
       }

    }

}