<?php


class imageThum {
  
  /*
	*图片的基本信息
	*/
   

    //内存中的图片
    
  public static function thum($src,$width,$height,$newname)
    {
       $info=getimagesize($src);
	   $info_to=array(
            'width'=>$info[0],
            'height'=>$info[1],
            'type'=>image_type_to_extension($info['2'],false),
	   	    'mime'=>$info['mime'],
	   	 );
	   $fun="imagecreatefrom{$info_to['type']}";
	   $timage=$fun($src);
       
       $image_thum=imagecreatetruecolor($width, $height);
       imagecopyresampled($image_thum, $timage, 0, 0, 0, 0, $width, $height, $info_to['width'], $info_to['height']);
       imagedestroy($timage);
       $timage=$image_thum;
       
       $funs="image{$info_to['type']}";
       $name=$newname.'.'.$info_to['type'];
       $funs($timage,$name); 
       //dirname(dirname(dirname(__FILE__)));
       return $name;
    }


}