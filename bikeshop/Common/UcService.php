<?php


    class Common_UcService{
		
    public function __construct(){
    require_once( 'conf/config_ucenter.php');
    require_once( 'uc_client/client.php');
    require_once('uc_client/imageThum.php');
    }
	
	    /**
    * 会员注册
    */
    public function register($username, $password, $email){
    $uid = uc_user_register($username, $password, $email);//UCenter的注册验证函数
    if($uid <= 0) {
    if($uid == -1) {
    return '用户名不合法';
    } elseif($uid == -2) {
      return '包含不允许注册的词语';
    } elseif($uid == -3) {
      return '用户名已经存在';
    } elseif($uid == -4) {
      return 'Email 格式有误';
    } elseif($uid == -5) {
      return 'Email 不允许注册';
    } elseif($uid == -6) {
      return '该 Email 已经被注册';
    } else {
      return '未定义';
    }
    } else {
      return intval($uid);//返回一个非负数
    }
    }
    
 
 /*
 会员登录验证
 */ 
	
    public function uc_login($username, $password){
      
    list($uid, $username, $password, $email) = uc_user_login($username, $password);
    if($uid > 0) {
    return array(
    'ucid'     => $uid,
    'username' => $username,
    'password' =>DI()->md5->md5_encode($password),
    'email' => $email,
    'pic'   =>uc_image_url($uid,null),
    );
    } elseif($uid == -1) {
    return '用户不存在,或者被删除';
    } elseif($uid == -2) {
    return '密码错误';
    } elseif($uid == -3) {
    return '安全提问错误';
    } else {
    return '未定义';
    }
    }	
/*
*查询用户名
*/
  public function uc_user_checkname($username){
   
     return uc_user_checkname($username);  
  }
/*
*获取用户信息
*$type 为[1]是用UID获取用户信息
*
*/  
  public function  uc_get_user($uid,$type){
       
        return uc_get_user($uid,$type); 
  }
/*
*同步登陆
*/	
    public function uc_synlogin($uid){
    return uc_user_synlogin($uid);
    }	

/*
*获取用户头像
*/    
  public function uc_image_url($uid,$size){

   return  uc_image_url($uid,$size);
  
  }
/*
*会员资料更新
*/   	
   public function uc_user_edit($username,$oldpw,$newpw,$email,$num){
     
     $ucresult = uc_user_edit($username, $oldpw, $newpw,$email,$num);
    if($ucresult == -1) {
        return '旧密码不正确';
     } elseif($ucresult == -4) {
        return 'Email 格式有误';
     } elseif($ucresult == -5) {
         return 'Email 不允许注册';
     } elseif($ucresult == -6) {
       return '该 Email 已经被注册';
     }elseif ($ucresult == 1) {
       return '修改成功';
     }elseif ($ucresult == 0) {
       return '修改成功';
     }
   }
/*
*修改用户名 
*/
    public function uc_user_merge($username,$usernamenew,$uid,$password,$email){
       $uid = uc_user_merge($username,$usernamenew,$uid,$password,$email);
       if($uid > 0) {
          return $uid;
      } elseif($uid == -1) {
          return '用户名不合法';
      } elseif($uid == -2) {
        return '包含要允许注册的词语';
      } elseif($uid == -3) {
        return '用户名已经存在';
    }
   }

 /*
 *移除用户 
 */   
     public function uc_user_delete($uid){
       return uc_user_delete($uid);
      }
	/*
  *进入消息中心
  */
     public function uc_pm_location($uid){
        uc_pm_location($uid);

     }
  /*
  *修改用户头像
  */
   public function uc_avatar($uid,$type){
   
    $url=uc_avatar_url($uid);
    $path="public/".time();
    $ter1=imageThum::thum($type['tmp_name'],200,200,$path);
    $ter2=imageThum::thum($type['tmp_name'],120,120,$path.'_m');
    $ter3=imageThum::thum($type['tmp_name'],48,48,$path.'_d');
    $formvars["avatar1"] =$this->imgTo16Code($ter1);
    $formvars["avatar2"] =$this->imgTo16Code($ter2);
    $formvars["avatar3"] =$this->imgTo16Code($ter3); 
    $data =$this->postXML($formvars,$url);
        @unlink($ter1);
        @unlink($ter2);
        @unlink($ter3);
       return  $data;
     
   } 
 /*
  *获取消息列表 
  *array uc_pm_list(integer uid [, integer page , integer pagesize , string folder , string filter, integer msglen])
  * @page当前页编号，默认值 1
  * @pagesize  每页最大条目数，默认值 10
  * @folder   短消息所在的文件夹 (newbox:新件箱inbox:(默认值) 收件箱 outbox:发件箱)
  * @filter  过滤方式(newpm:(默认值) 未读消息，folder 为 inbox 和 outbox 时使用)
   * @msglen 截取短消息内容文字的长度，0 为不截取，默认值 0     
  */ 
    public function uc_pm_list($uid,$pade){
      $news= uc_pm_list($uid,$pade);
       
      return $news; 
    }   
 /*
  *发送消息
  *@fromuid  发件人用户 ID
  *@msgto 收件人用户名 / 用户 ID，多个用逗号分割
  *@ subject  消息标题
  *@message 消息内容
  *@  instantly 是否直接发送1:(默认值) 直接发送消息0:进入发送短消息的界面
  *@ isusername 1:msgto 参数为用户名0:(默认值) msgto 参数为用户 ID
  *@ type 1:群聊消息 0:(默认值)私人消息
  */
   public function  uc_pm_send($fromuid,$msgto,$subject,$message,$arr ){
   
    return  uc_pm_send($fromuid,$msgto,$subject,$message,$arr);
 
   }
 /*
 *会话消息内容
 *@pmid 消息ID
 */
 
   public function  uc_pm_view($uid,$pmid,$arr){
  
     return uc_pm_view($uid,$pmid,$arr);  

   } 
 /*
 *单条消息内容
 */
   public function  uc_pm_viewnode($uid,$tyoe=0,$pmid){
     
     return  uc_pm_viewnode($uid,$tyoe,$pmid);

   }
 /*
 *删除消息
 */
  public function  uc_pm_delete($uid,$floder="inbox",$pmids){
     
     return    uc_pm_delete($uid,$floder,$pmids);
   }
  /*
   *删除会话
   */
  public function uc_pm_deleteuser($uid , $touids){

    return uc_pm_deleteuser($uid , $touids);
  }
 
 /*
 *发送远程接口
 */
 protected function postXML($xml, $url, $useCert = false, $second = 30) {
       $ch = curl_init();
    //设置超时
    curl_setopt($ch, CURLOPT_TIMEOUT, $second);
    
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
    //设置header
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    //要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //post提交方式
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    //运行curl
    $data = curl_exec($ch); 
   
      if($data){
        curl_close($ch);
        return true; //$this->xml_to_data($data);
      } else { 
        $error = curl_errno($ch);
        curl_close($ch);
        return false;
       }
    } 
   /* 把图片转换成16进制
       * @file : 文件路径
     * */
      public  function imgTo16Code($filename){
        
          $file = file_get_contents($filename);
          $code = strtoupper(bin2hex($file));
          return $code;
       }
     /**
     * 将xml转为array
     * @param string $xml
   * return array
     */
  public function xml_to_data($xml){  
    if(!$xml){
      return false;
    }
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);    
    return $data;
  }

       
 }   