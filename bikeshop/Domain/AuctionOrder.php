<?php

   /**
 	*拍卖订单 
 	*/
 	class Domain_AuctionOrder 
 	{
 		
	 	  public function add($data)
	 	  {  
	 	  	$model=new Model_AuctionOrder();

	        return $model->add($data); 
	 	  }

	       public  function updated($gid,$uid,$data){
             
              $model=new Model_AuctionOrder();

	              return $model->updated($gid,$uid,$data); 

	       } 

	      public function getMoney($gid,$uid){
                  
                  $model=new Model_AuctionOrder();

	              return $model-> getMoney($gid,$uid);
	      	 
	      } 

        public function getMax($gid){
                  
                  $model=new Model_AuctionOrder();

	              return $model-> getMax($gid);
	      	 
	      } 
         //查询拍卖商品
	    public function getGood($gid,$select){
            
            $model=new Model_SpecialGood();
	        
	        return  $model->getAll($gid,$select);
	     }

	    public function getPai($uid,$status){
           
           $model=new Model_AuctionOrder();

	         return $model->getPai($uid,$status);

	    }   
      public function  offerList($gid){
         
         $model=new Model_AuctionOrder();

	      return $model->offerList($gid); 
      }   
 	} 	