<?php 


class Domain_SpecialGood {
 
  public function add($data){
    
    $model=new Model_SpecialGood();

    return $model->add($data);
  }
  
  public  function updated($id,$data){
    
     $model=new Model_SpecialGood();   

    return $model->updated($id,$data);
  }
  
   public  function deleteGood($id){
    
     $model=new Model_SpecialGood();   

    return $model->deleteGood($id);
  } 
 
  public  function getAll($id,$select){
    
     $model=new Model_SpecialGood();   

    return $model->getAll($id,$select);
  }
 
   public  function getShopList($shop){
    
     $model=new Model_SpecialGood();   

    return $model->getShopList($shop);
  }

  public  function getNum($id){
    
     $model=new Model_SpecialGood();   

    return $model->getNum($id);

  }

  //获取门店名称
      public function getName($id){
        $model = new Model_HomePageShop();

        return $model->getName($id);
    }
  //获取报名人数 
   public function getBao($gid){
      
        $model=new Model_AuctionOrder();
       
        return $model->getNum($gid); 
    }
}