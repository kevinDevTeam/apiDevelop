<?php

/**
*
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/22
 * Time: 19:36
 */
class Domain_RegisterUser {

    //验证用户信息
    public function user($key,$value) {
       
        $model = new Model_RegisterUser();

        return $model->getUser($key,$value);
    }
  
  public function inBaseUser($data){

      $model =new Model_RegisterUser();
      
      return  $model->inUser($data);
  }
  //用户完善资料
   public function perfectInfo($uid,$data){
      
       $model =new Model_RegisterUser();
 
        return  $model->perfectInfo($uid,$data);
   } 
 //插入验证码
   public function inCode($data){

        $model =new Model_RegisterUser();

        return  $model->inCode($data);
   }
  //验证验证码
   public function VerificationCode($phone,$code){
       
       $model =new Model_RegisterUser();

       return  $model->VerificationCode($phone,$code);
   }

  public function getPwd($phone){
    $model =new  Model_RegisterUser();

    return $model->getPwd($phone);

  } 
 public function upPwd($phone,$password){
    $model =new  Model_RegisterUser();

    return $model->upPwd($phone,$password);

} 


}