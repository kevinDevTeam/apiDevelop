<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 15:04
 */
class Domain_ShopCart {

    //插入购物车数据
    public function shopCart($data){

        $model = new Model_ShopCart();

        return $model->shopCart($data);
    }

    //根据商品id获取图片
    public function getByGid($uid,$gid){

        $model = new Model_ShopCart();

        return $model->getByGid($uid,$gid);
    }

    //根据id uid sid 查询数据
    public function getByIdUid($field,$data){
        $model = new Model_ShopCart();

        return $model->getByIdUid($field,$data);
    }

    //根据uid获取列表内容
    public function getList($uid){
        $model = new Model_ShopCart();

        return $model->getList($uid);
    }

    //根据gid更新number的值
    public function updateNumber($field,$number){
        $model = new Model_ShopCart();

        return $model->updateNumber($field,$number);
    }

    //根据uid id gid 更新购物车数据
    public function updateData($id,$data){
        $model = new Model_ShopCart();

        return $model->updateData($id,$data);
    }

    //根据 id data 更新购物车数据
    public function upDisplay($id,$data){
        $model = new Model_ShopCart();

        return $model->upDisplay($id,$data);
    }

    //根据id查
    public function getCart($id){
        $model=new Model_ShopCart();

        return $model->getCart($id);
    }

     //根据id查
    public function getall($id){
        $model=new Model_ShopCart();

        return $model->getall($id);
    }

     //根据sid查
    public function getCartId($sid){
        
        $model=new Model_ShopCart();

        return $model->getCartId($sid);

    }
    //根据uid id 删除购物车
    public function deleteData($id){
        $model = new Model_ShopCart();

        return $model->deleteData($id);
    }

    //统计购物车总数
    public function countShopCart($uid){
        $model = new Model_ShopCart();

        return $model->countShopCart($uid);
    }

    //根据uid获取 nature_val 值
    public function getByUidValues($uid){
        $model = new Model_ShopCart();

        return $model->getByUidValues($uid);
    }

    //根据属性值,查询id
    public function getNatureById($nature){
        $model = new Model_ShopCart();

        return $model->getNatureById($nature);
    }

    //根据id查询nature_val值
    public function getByIdNature($id){
        $model = new Model_ShopCart();

        return $model->getByIdNature($id);
    }


    //根据uid+id+购物车id查询数据
    public function getBYUidIdSId($id,$uid){
        $model = new Model_ShopCart();

        return $model->getBYUidIdSId($id,$uid);
    }

    //根据uid+id+购物车id查询数据
    public function getBySid($sid){
        $model = new Model_ShopCart();

        return $model->getBySid($sid);
    }






}