<?php

/**
*
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 10:36
 */
class Domain_TheOrder {

    //
    public function order($uid,$status) {
       
        $model = new Model_TheOrder();

        return $model->getOrder($uid,$status);
    }
   
   public function orderAll($id){
       
       $model= new Model_TheOrder();

       return $model->orderAll($id);

   }
   
   public function inOrder($data){

      $model =new Model_TheOrder();
      
      return  $model->inOrder($data);
  }
   public function getAccept($id){

      $model =new Model_TheOrder();
      
      return  $model->getAccept($id);
  }

   public function getNid($data){

      $model =new Model_NatureVal();
      
      return  $model->getByIdUidNatureVal($data);
  }
           

 public function  getByNIdNature($nid){

      $model =new Model_Nature();
      
      return  $model-> getByNIdNature($nid);
  }

  public function upOrder($id,$data){
    
      $model =new Model_TheOrder();
      
      return  $model->upOrder($id,$data);

  }
  public function delOrd($id){

    $model =new Model_TheOrder();
      
   return  $model->delOrd($id);
  }
  //获取银行利率
  public function getRate($id){

     $model=new Model_Bank();

     return $model->getRate($id);
  }
  //获取默认银行利率
  public function getRateOne(){

     $model=new Model_Bank();

     return $model->getRateOne();
  }
  //获取银行介绍
  public function getInfo($bank){
    $model=new Model_Bank();

     return $model->getInfo($bank);

  }
  public function toolFee($trade){

    $model =new Model_TheOrder();
      
   return  $model->toolFee($trade);

  }
  //获取银行
  public function getBank(){

    $model =new Model_Bank();
      
   return  $model->getBank();

  }
  //查询全额、定金支付
  public function getType($trade){

      $model =new Model_TheOrder();  
    
    return  $model->getType($trade);
  }
  public function getCount($trade){
    
    $model =new Model_TheOrder();  
    
    return  $model->getCount($trade);

  }
  //查询提货码
  public  function getDelivery($oid){
    
    $model =new Model_TheOrder();  
    
    return  $model->getDelivery($oid); 
  }
   //根据提货码查询
  public function verifyCode($uid,$code){
     
      $model =new Model_TheOrder();  
    
     return  $model->verifyCode($uid,$code); 

  }

}