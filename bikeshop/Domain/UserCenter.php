<?php

/**
*
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/24
 * Time: 16:36
 */
class Domain_UserCenter {

    //验证用户信息
    public function user($username) {
       
        $model = new Model_UserCenter();

        return $model->getUser($username);
    }

   public  function getPwd($uid){

       $model = new Model_UserCenter();

       return $model->getPwd($uid);
    } 

   public  function getName($username){

       $model = new Model_UserCenter();

       return $model->getName($username);
    }  

  public  function getPic($uid){
  	$model = new Model_UserCenter();

    return $model->getPic($uid);
  }

   public function getShop($uid){
       $model = new Model_UserCenter();
  
      return $model->getShop($uid);
    }
 
    public function getAuthenticate($uid){
  
     $model = new Model_UserCenter();
  
     return $model->getAuthenticate($uid);
   } 

   public function upPwd($uid,$password){

        $model =new Model_UserCenter();
      
      return  $model->upPwd($uid,$password);
    }

   public function upName($uid,$username){

        $model =new Model_UserCenter();
      
      return  $model->upName($uid,$username);
 
    }

   public function upload($uid,$pic){
    
      $model =new Model_UserCenter();
      
      return  $model->upload($uid,$pic);

   }

   public function upPhone($uid,$phone){
    
      $model =new Model_UserCenter();
      
      return  $model->upload($uid,$phone);

   }


    public function inAuthenticate($data){
    
     $model=new Model_UserCenter();

      return $model->inAuthenticate($data);
     
    }
  
   public function Authenticate($uid){
    
     $model=new Model_UserCenter();

      return $model->Authenticate($uid);
     
    }
  public function getShopNum($uid){
      
      $model=new  Model_HomePageShop();

      return $model->getShopNum($uid); 
  }
 //检测版本接口
  public function getEdition(){
   
     $model=new Model_UserCenter();
    
     return  $model->getEdition(); 
  }

  public function testEdition($ver){
   
    $model=new Model_UserCenter();
   
    return  $model->testEdition($ver); 
  }

  public function updowloadNUm($id,$data){

     $model=new Model_UserCenter();
   
    return  $model->updowloadNUm($id,$data); 
  }
 
  public function userNum(){

     $model=new Model_UserCenter();

   return  $model->countUser();
 } 
}