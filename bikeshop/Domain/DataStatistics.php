<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/24
 * Time: 10:52
 */

class Domain_DataStatistics {
  
  //统计用户数量
  public function countUser(){
    $model = new Model_UserCenter();
   
    return $model->countUser();
  }
   //统计商品数量
   public function countGood(){
    $model = new Model_HomePageProducts();
   
    return $model->countGood();
  }
   //统计交易数量
   public function countOrder(){
    $model = new Model_TheOrder();
   
    return $model->countOrder();
  }
  
  //获取商品品牌
    public function getList(){

    $model = new Model_Brand();
   
    return $model->getList();
  }
 
 //获取推荐店铺
  public function getShop(){
   
    $model=new Model_HomePageShop();
      
    return $model->getShopAll();

  }



}