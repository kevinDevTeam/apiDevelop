<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/22
 * Time: 14:04
 */
class Domain_HomePageProducts
{

    //根据category查询属性名
    public function getByIdNature($cid)
    {
        $model = new Model_Nature();

        return $model->getByIdNature($cid);
    }

    //根据n_id查询属性名
    public function getByNIdNature($cid)
    {
        $model = new Model_Nature();

        return $model->getByNIdNature($cid);
    }

    //根据n_id 查询属性值
    public function getByIdNatureVal($nid)
    {
        $model = new Model_NatureVal();

        return $model->getByIdNatureVal($nid);
    }

    //根据c_id 查询属性值
    public function getByCIdNature($cid){
        $model = new Model_Nature();

        return $model->getByCIdNature($cid);
    }

    //新查询方法: 根据n_id查询属性名
    public function getByNIdNatureNew($arg)
    {
        $model = new Model_Nature();

        return $model->getByNIdNatureNew($arg);
    }

    //新查询方法: 传入字段值,查询值类似in
    public function getByIdNatureValNew($arg){
        $model = new Model_NatureVal();

        return $model->getByIdNatureValNew($arg);
    }

    //新查询方法: 根据n_id 查询属性值
    public function getNatureValByIdNid($nature)
    {
        $model = new Model_NatureVal();

        return $model->getNatureValByIdNid($nature);
    }

    //根据nature值 查询id和u_id
    public function getByNature($nature)
    {
        $model = new Model_NatureVal();

        return $model->getByNature($nature);
    }

    //根据nature值 查询id和u_id
    public function getByNatureNew($data)
    {
        $model = new Model_NatureVal();

        return $model->getByNatureNew($data);
    }


    //根据id 和n_id 查询values值
    public function getByIdUidNatureVal($data)
    {
        $model = new Model_NatureVal();

        return $model->getByIdUidNatureVal($data);
    }

    //新查询方法: 根据字段 查询所有数据
    public function getDataByField($field,$arg){

        $model = new Model_NatureVal();

        return $model->getDataByField($field,$arg);
    }


    //根据id查询详细值
    public function getByIdDetails($id, $field)
    {
        $model = new Model_HomePageProducts();

        return $model->getByIdDetails($id, $field);
    }

    //根据条件删选内容
    public function screening($data)
    {
        $model = new Model_HomePageProducts();

        return $model->screening($data);
    }

    //初始都没选择的时候
    public function getDefaultList($page)
    {
        $model = new Model_HomePageProducts();

        return $model->getDefaultList($page);
    }

    //根据 gid 查询 数据  收藏 俱乐部详情 获取门店上架数据 有用
    public function getByGid($gid, $field,$page)
    {

        $model = new Model_HomePageProducts();

        return $model->getByGid($gid, $field,$page);
    }

    //获取id，title，retail_prices，image_url 字段
    public function products($page)
    {
        $model = new Model_HomePageProducts();

        return $model->getProducts($page);
    }

    //模糊查询商品
    public function search($field, $data)
    {
        $model = new Model_HomePageProducts();

        return $model->search($field, $data);
    }

    //根据商品id获取earnest
    public function earnest($gid){
        $model = new Model_HomePageProducts();

        return $model->earnest($gid);
    }

    //根据商品id 查询部分数据
    public function getByIdData($id,$field){
        $model = new Model_HomePageProducts();

        return $model->getByIdData($id,$field);
    }

    //上架商品
    public function add($data)
    {
        $model = new Model_Shelves();

        return $model->add($data);
    }

    //更新number值
    public function editNumber($arg,$data){
        $model = new Model_Shelves();

        return $model->editNumber($arg,$data);
    }


    //根据字段查询值
    public function getByIdSidNatureVal($field,$data){
        $model = new Model_Shelves();

        return $model->getByIdSidNatureVal($field,$data);
    }

    //根据id uid sid nature 查询库存量
    public function getByIdSidNumber($field,$data){
        $model = new Model_Shelves();

        return $model->getByIdSidNumber($field,$data);
    }

    //根据uid sid gid查询上架内容
    public function getByUidSidContent($field,$arg){
        $model = new Model_Shelves();

        return $model->getByUidSidContent($field,$arg);
    }

    //查询skt表数据
    public function getByIdValue($id){
        $model = new Model_ProductsSku();

        return $model->getByIdValue($id);
    }

  //查询sku组合
   public function getSku($id){
     $model=new Model_ProductsSku();

      return $model->getNature($id);
   }

    public function getDataByFieldSku($field,$arg){
        $model=new Model_ProductsSku();

        return $model->getDataByFieldSku($field,$arg);
    }

}




































