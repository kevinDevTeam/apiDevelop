<?php

/**
 * 收藏管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/18
 * Time: 9:17
 */
class Api_Collect extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'add' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => "用户ID"],
                'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true, 'desc' => "商品ID"],
            ],
            'getList' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => "用户ID"],
                'page' => ['name' => 'page', 'type' => 'int', 'require' => true, 'desc' => "分页"],

            ],
            'delete' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => "用户ID"],
                'ids' => ['name' => 'ids', 'type' => 'array', 'format' => 'explode', 'require' => false, 'separator' => ',', 'desc' => '收藏id,可删除一个或者多个']
            ],
        ];
    }

    /**
     * 添加收藏
     * @desc 添加收藏
     * @return boolean add true表示成功/false表示失败
     */
    public function add()
    {
        //接受uid  gid 写入
        $collect = new Domain_Collect();
        $gid = $collect->getByUid($this->uid, 'id,gid');

        foreach ($gid as $v) {
            if ($this->gid == $v['gid']) {
                return $collect->delete($v['id']) ? false : [];
            }
        }

        $data = [
            'uid' => $this->uid,
            'gid' => $this->gid,
            'time' => date('Y-m-d H:i:s')
        ];

        return $collect->add($data) ? true : false;
    }

    /**
     * 收藏列表
     * @desc 收藏列表
     * @return string title 自行车标题
     * @return string image_url 图片地址
     * @return string retail_price 价格
     * @return string id 商品id
     * @return string time 收藏时间
     */
    public function getList()
    {
        //根据uid,读取出gid查询
        $collect = new Domain_Collect();

        $gid = $collect->getByUid($this->uid, 'gid');  //获取gid
        $a = array_column($gid, 'gid');

        for ($i = 0; $i < count($a); $i++) {
            $collectData[] = $collect->getCollectData($a[$i], $this->uid); //根据uid获取  id time

            $arr[] = $collectData[$i][0];
        }

//        for($i=0;$i<count($collectData);$i++){
//            $arr[] = $collectData[$i][0];
//        }
//        return count($arr);


        if ($this->page) {
            for ($i = 0; $i < $this->page * 4; $i++) {
                @$arr2[] = $arr[$i];
                @$arr2[$i]['collectId'] = $arr[$i]['id'];
                @$arr2[$i]['id'] = $arr[$i]['gid'];
                if(!$arr2[$i]['collectId']){
                    unset($arr2[$i]);
                }
                unset($arr2[$i]['gid']);
            }
        }

        return $arr2;

    }

    /**
     * 删除收藏
     * @desc 删除收藏
     * @return boolean add true表示成功/false表示失败
     */
    public function delete()
    {
        //根据id可批量删除
        $collect = new Domain_Collect();

        $count = count($this->ids);  //统计数量

        for ($i = 0; $i < $count; $i++) {

            $rs[] = $collect->delete($this->ids[$i]);
        }

        return [
            'isDelete' => $rs ? true : false,
        ];
    }
}

