<?php

/**
 * 拍卖接口（2.0） -
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/14
 * Time: 14:00
 */
class Api_AuctionOrder extends PhalApi_Api
{
    public function getRules()
    {
        return [
            'signUp' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true,  'desc' => '用户id'],
                'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true,'desc' => '商品id'],
                'bond' => ['name' => 'bond', 'type' => 'string', 'require' => true,'desc' => '保证金'],
                'shop_user' => ['name' => 'shop_user', 'type' => 'int', 'require' => true,'desc' => '商家ID'],
            ],
            'addMoney'=>[
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
                'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true,'desc' => '商品id'],
                'money' => ['name' => 'moeney', 'type' => 'int', 'require' => true,'desc' => '出价（加价）金额'],
           ],
          'getMaxMomey'=>[
              'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
              'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true,'desc' => '商品id'],
          ],
          'orderList' =>[
              'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
              'status' => ['name' => 'status', 'type' => 'int', 'require' => true,'desc' => '订单状态'], 
          ],
          'offrList' =>[
            'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true,'desc' => '商品id'],
          ],
        ];
    }
   
   /**
    *拍卖报名
    *@desc拍卖报名
    *@return string id 订单号
    */
    public  function  signUp(){
       
       $domain=new Domain_AuctionOrder();
       $data=array(
          'uid'      =>$this->uid,
          'g_id'      =>$this->gid,
          'bond'     =>$this->bond,
          'shop_user'=>$this->shop_user,
          'time'     =>date('Y-m-d H:i:s'), 
       	);
      return $domain->add($data);
    }  
   /**
    *拍卖出价+加价
    *@desc拍卖出价++加价
    * @return boolean addMomey true表示成功/false表示失败
    */
    public function addMoney(){
        
       $domain=new Domain_AuctionOrder();
       $data=array(
          'money'   =>$this->money,
          'add_time'=>date("Y-m-d H:i:s"),
       	);
       return $domain->updated($this->uid,$this->gid,$data);
    }
  
  /**
   *查询当前出价和最高出价
   @desc 查询当前出价和最高出价
   *@return string old_money  历史出价
   *@return string max_money  当前最高出价
   */ 
   public function getMaxMomey(){

     $domain=new Domain_AuctionOrder();
     $re=$domain->getMax($this->gid);
     $rt=$domain->getMoney($this->uid,$this->gid);
     $data=[
      'old_money'=>$rt['money'],
      'max_momey'=>$re,
     ]; 
    return $data;
   }
  
  /**
   *我的拍卖列表
   @desc 我的 拍卖列表
   *@return string oid  订单号
   *@return string status  订单状态 0未付款 1已报名 2已结束 3待付尾款 4待受理 5待提货
   *@return string max_money  最大成交价
   *@return array good 商品信息
   */ 

     public function orderList(){
       $domain=new Domain_AuctionOrder(); 
       $re=$domain->getPai($this->uid,$this->status);
       $data=array();
       foreach ($re as $ke) {
          $good=$domain->getGood($ke['g_id'],'title,image_url,sign,brand,sort,price,cost_price,start_time,end_time');
          $max=$domain->getMax($ke['g_id']);
          $date=[
            'oid'   =>$ke['o_id'],
            'good'  =>$good,
            'status'=>$ke['status'],
            'max_money'=>$max,
          ];
          $data[]=$date; 
        } 
       return $data;  
    }
   /**
   *出价记录
   *@desc 出价记录
   *@return string money  出价
   *@return string add_time  时间
   *@return string username  用户名
   */  
   public function offrList(){
     $domain=new Domain_AuctionOrder();
     $date=$domain->offerList($this->gid);
     return $date;
   }

}