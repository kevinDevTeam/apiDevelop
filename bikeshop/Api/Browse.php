<?php

/**
 * 浏览记录-
 * Date: 2016/10/8
 * Time: 14:30
 */
class Api_Browse extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'addBrowse' => [
                'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true, 'desc' => "商品ID"],

                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => "用户ID"],
            ],
            'getBrowse' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => "用户ID"],
                'page' => ['name' => 'page', 'type' => 'int', 'require' => true, 'desc' => "当前页数"],
            ],


            'detele' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => "用户id"],
            ],
            'getStatus' => [
                'gid' => ['name' => 'gid', 'type' => 'int', 'require' => true, 'desc' => "商品ID"],

                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => "用户ID"],

            ]
        ];
    }


    /**
     *生成浏览记录
     * @desc 浏览记录生成
     * @return int code 操作码，201表示提交成功，  401提交失败
     * @return string msg 操作码  提交成功  提交失败
     *
     */
    public function addBrowse()
    {
        $rs = array();
        $domain = new Domain_Browse();
        $data = [
            'zixc_bikeshop_goods_id' => $this->gid,
            'uid' => $this->uid,
            'time' => date("Y-m-d"),
        ];

        $arg = [
            'zixc_bikeshop_goods_id' => $this->gid,
            'uid' => $this->uid,
        ];
        $response = $domain->getFieldByData('id', $arg);
        //判断是否存在，存在更新。
        if (!$response) {
            if ($domain->inBrowse($data)) {
                $rs['code'] = 201;
                $rs['msg'] = "提交成功";
                return $rs;
            } else {
                $rs['code'] = 401;
                $rs['msg'] = "提交失败";
                return $rs;
            }
        } else {
            //不存在则提交
            $editData = [
                'time' => date("Y-m-d"),
            ];
            $domain->getFieldEdit($arg, $editData);
            $rs['code'] = 201;
            $rs['msg'] = "编辑成功";
            return $rs;
        }

    }


    /**
     *浏览记录
     * @desc 浏览记录清空
     * @return sting msg  清空成功  清空失败
     * @return int  code  203清空成功   403清空失败
     */
    public
    function detele()
    {

        $domain = new Domain_Browse();

        if ($domain->detele($this->uid)) {
            $rs['code'] = "203";
            $rs['msg'] = T("删除成功");
            return $rs;
        } else {
            $rs['code'] = "403";
            $rs['msg'] = T("删除失败");
            return $rs;
        }
    }


    /**
     *浏览记录查询
     * @desc 浏览记录查询
     * @return sting title  商品名称
     * @return sting retail_price 商品单价
     * @return int id 商品id
     * @return sting  image_url 商品图片
     * @return time   时间
     */
    public
    function  getBrowse()
    {
        $domain = new Domain_Browse();
        $start = 0;
        $end =  10*$this->page;
        $data = $domain->Browse($this->uid, $start, $end);
        return $data;
    }

    /**
     *购物车数量/商品收藏状态
     * @desc 购物车数量 商品收藏状态查询
     * @return int  goodnum 购物车数量
     * @return int   collectStatus  该商品是否被收藏 1表示收藏
     */


    public
    function getStatus()
    {
        $domain = new Domain_Login();
        $domin = new Domain_Collect();
        $num = $domain->getNum($this->uid);
        if ($domin->getStatus($this->uid, $this->gid)) {
            $staus = 1;
        } else {
            $staus = 0;
        }
        $data = array(
            "goodnum" => intval($num, 10),
            "collectStatus" => $staus,
        );
        return $data;

    }


}





































































