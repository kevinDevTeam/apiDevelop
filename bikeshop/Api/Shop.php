<?php

/**
 * 门店管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/23
 * Time: 11:13
 */
class Api_Shop extends PhalApi_Api
{

    public function getRules()
    {
        return [
            'add' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => false, 'desc' => '用户id'],
                'logo' => [
                    'name' => 'logo', 'type' => 'file', 'min' => 0, 'max' => 1024 * 1024, 'require' => true, 'range' => array('image/jpg', 'image/jpeg', 'image/png'),
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc' => '门店logo'
                ],
                'image_url' => [
                    'name' => 'image_url', 'type' => 'file', 'min' => 0, 'max' => 1024 * 1024, 'require' => true, 'range' => array('image/jpg', 'image/jpeg', 'image/png'),
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc' => '门店轮播'
                ],
                'parameter' => ['name' => 'parameter', 'type' => 'array', 'require' => false, 'format' => 'json', 'desc' => '{"tel": "135565656","title": "标题","address": "地址","shop_type": "品牌车店","brand_type": "可选","store_size": "200","shop_time": "16:29-17:29","trade": "5,可选","brief":"门店简介"}'],
            ],
            'edit' => [
                'sid' => ['name' => 'sid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '门店id,更新的时候传入'],
                'logo' => [
                    'name' => 'logo', 'type' => 'file', 'min' => 0, 'max' => 1024 * 1024, 'require' => true, 'range' => array('image/jpg', 'image/jpeg', 'image/png'),
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc' => '门店logo'
                ],
                'image_url' => [
                    'name' => 'image_url', 'type' => 'file', 'min' => 0, 'max' => 1024 * 1024, 'require' => true, 'range' => array('image/jpg', 'image/jpeg', 'image/png'),
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc' => '门店轮播'
                ],
                'parameter' => ['name' => 'parameter', 'type' => 'array', 'require' => true, 'format' => 'json', 'desc' => '编辑参数/{"tel":111}/{"title":111}/{"shop_time":2016-12-29 14:58:08}'],
            ],
            'updateLogo' => [
                'id' => ['name' => 'id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '门店id'],
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
                'logo' => [
                    'name' => 'logo', 'type' => 'file', 'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'),
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc' => '门店logo'
                ],
            ],
            'delete' => [
                'id' => ['name' => 'id', 'type' => 'string', 'require' => true, 'desc' => '门店id'],
            ],
            'shopList' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
            ],
            'review' => [
                'sid' => ['name' => 'sid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '门点id'],
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户名'],
                'message' => ['name' => 'message', 'type' => 'string', 'require' => true, 'desc' => '留言内容'],
                'score' => ['name' => 'score', 'type' => 'string', 'require' => true, 'desc' => '评分'],
            ],
            'reviewList' => [
                'sid' => ['name' => 'sid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '商品id'],
            ],
            'shopDetails' => [
                'sid' => ['name' => 'sid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '门店id'],
            ],
            'screening' => [
                'shop_type' => ['name' => 'shop_type', 'type' => 'string', 'require' => false, 'desc' => '门店类型'],
                'brand_type' => ['name' => 'brand_type', 'type' => 'string', 'require' => false, 'desc' => '选择品牌'],
                'store_size' => ['name' => 'store_size', 'type' => 'string', 'require' => false, 'desc' => '需要传一个默认值DESC,门店规模,从大到小传DESC,从小到大传ASC'],
            ],
            'shelves' => [
                'id' => ['name' => 'id', 'type' => 'string', 'require' => true, 'desc' => '门店id'],
            ],
            'number' => [
                'sid' => ['name' => 'sid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '俱乐部id'],
            ],
            'getGidShopList' => [
                'gid' => ['name' => 'gid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '商品id'],
                'longitude' => ['name' => 'longitude', 'type' => 'string', 'require' => true, 'desc' => '经度'],
                'latitude' => ['name' => 'latitude', 'type' => 'string', 'require' => true, 'desc' => '纬度'],
            ],
            'getGidScreening' => [
                'gid' => ['name' => 'gid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '商品id'],
                'star' => ['name' => 'star', 'type' => 'string', 'require' => false,  'desc' => '星级,从大到小传DESC,从小到大传ASC'],
                'distance' => ['name' => 'distance', 'type' => 'string', 'require' => false,  'desc' => '距离远近,从大到小传DESC,从小到大传ASC'],
                'longitude' => ['name' => 'longitude', 'type' => 'string', 'require' => true, 'desc' => '经度'],
                'latitude' => ['name' => 'latitude', 'type' => 'string', 'require' => true, 'desc' => '纬度'],
            ],
            'getShopBaseInfo' => [
                'id' => ['name' => 'id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '门店id'],
                'uid' => ['name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户id'],
            ],
            'shopDataScreening' => [
                'sid' => ['name' => 'sid', 'type' => 'int', 'require' => true, 'desc' => '门店id '],
                'category' => ['name' => 'category', 'type' => 'string', 'require' => false, 'desc' => '分类名'],
                'brand' => ['name' => 'brand', 'type' => 'string', 'require' => false, 'desc' => '品牌名'],
                'retail_price' => ['name' => 'retail_price', 'type' => 'string', 'require' => false, 'desc' => '商品价格区间,"从大到小","从小到大"'],
                'page' => ['name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '页数/默认1'],
            ],
                'getShopInfo'=>[
                'sid' => ['name' => 'sid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '门店id'],
            ]
        ];
    }

    /**
     * 门店添加
     * @desc 如果要更新,要参入id值
     * @return boolean isUpdate/isInsert true表示成功/false表示成功
     */
    public function add()
    {
        $key = DI()->config->get('app.geocode');

        $gaoInfo = file_get_contents('http://restapi.amap.com/v3/geocode/geo?key=' . $key['key'] . '&' . 'address=' . $this->parameter['address']);//根据高德API查询,地理位置

        $gaoInfoDe = json_decode($gaoInfo, true); //编码json字符串
        $location = explode(',', $gaoInfoDe['geocodes'][0]['location']); //根据逗号分割经纬度

        $shop = new Domain_HomePageShop();

        $dst = DI()->helper->getFileName('uploads', $this->logo); //获取logo的上传路径
        $dst2 = DI()->helper->getFileName('uploads', $this->image_url); //获取image_url的上传路径

        $logoData = DI()->cosSdk->uploadCos('bikeshop', $this->logo['tmp_name'], $dst);
        $imageData = DI()->cosSdk->uploadCos('bikeshop', $this->image_url['tmp_name'], $dst2);

        $logoScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $logoData['data']['source_url']);
        $imageScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $imageData['data']['source_url']);

        $data = [
            'uid' => $this->uid,
            'tel' => $this->parameter['tel'],
            'title' => $this->parameter['title'],
            'address' => $this->parameter['address'],
            'shop_type' => $this->parameter['shop_type'],
            'brand_type' => $this->parameter['brand_type'],
            'store_size' => $this->parameter['store_size'],
            'shop_time' => $this->parameter['shop_time'],
            'logo' => $logoScore,
            'image_url' => $imageScore,
            'longitude' => $location[0],
            'latitude' => $location[1],
            'trade' => $this->parameter['trade'] ? $this->parameter['trade'] : 0,
            'brief' => $this->parameter['brief'],
        ];

        return $shop->add($data) ? true : false;
    }

    /**
     * 获取商品品牌
     * @desc 获取商品品牌
     * @return string logo 商品logo
     * @return string brand 商品品牌
     */
    public function listShopBrand()
    {
        $brand = new Domain_Brand();

        return $brand->getList();
    }

    /**
     * 编辑门店
     * @desc 根据用户,id 编辑门店
     * @return boolean isUpdate/isInsert true表示成功/false表示成功
     */
    public function edit()
    {
        $shop = new Domain_HomePageShop();

//        $json = json_encode($this->parameter, JSON_UNESCAPED_UNICODE); //编码参数成json

//        $request = json_decode($json, true); //转成数组形式

//        DI()->logger->debug('编辑门店所有值:', json_encode($this, JSON_UNESCAPED_UNICODE));
//        DI()->logger->debug('编辑门店参数值:', json_encode($this->parameter, JSON_UNESCAPED_UNICODE));

//        if (!empty($request['tel'])) {
//            $params = [
//                'tel' => $request['tel'],
//            ];
//        }
//
//        if (!empty($request['title'])) {
//            $params = [
//                'title' => $request['title'],
//            ];
//        }
//
//        if (!empty($request['shop_time'])) {
//            $params = [
//                'shop_time' => $request['shop_time'],
//            ];
//        }

        $key = DI()->config->get('app.geocode');

        $gaoInfo = file_get_contents('http://restapi.amap.com/v3/geocode/geo?key=' . $key['key'] . '&' . 'address=' . $this->parameter['address']);//根据高德API查询,地理位置

        $gaoInfoDe = json_decode($gaoInfo, true); //编码json字符串
        $location = explode(',', $gaoInfoDe['geocodes'][0]['location']); //根据逗号分割经纬度

        $dst = DI()->helper->getFileName('uploads', $this->logo); //获取logo的上传路径
        $dst2 = DI()->helper->getFileName('uploads', $this->image_url); //获取image_url的上传路径

        $logoData = DI()->cosSdk->uploadCos('bikeshop', $this->logo['tmp_name'], $dst);
        $imageData = DI()->cosSdk->uploadCos('bikeshop', $this->image_url['tmp_name'], $dst2);

        $logoScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $logoData['data']['source_url']);
        $imageScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $imageData['data']['source_url']);

        $params = [
            'tel' => $this->parameter['tel'],
            'title' => $this->parameter['title'],
            'address' => $this->parameter['address'],
            'shop_type' => $this->parameter['shop_type'],
            'brand_type' => $this->parameter['brand_type'],
            'store_size' => $this->parameter['store_size'],
            'shop_time' => $this->parameter['shop_time'],
            'logo' => $logoScore,
            'image_url' => $imageScore,
            'longitude' => $location[0],
            'latitude' => $location[1],
            'trade' => $this->parameter['trade'] ? $this->parameter['trade'] : 0,
            'brief' => $this->parameter['brief'],
        ];
        $arg = [
            'id' => $this->sid
        ];

        $rs = $shop->edit($params,$arg);
        return [
            'isUpdate' => $rs ? true : false,
        ];
    }

    /**
     * 获取门店信息
     */
    public function getShopInfo(){
        $shop = new Domain_HomePageShop();
        $field = 'id,title,logo,image_url,shop_type,brand_type,store_size,tel,address,shop_time,trade,brief';
        $arg = [
            'id' => $this->sid
        ];
        $rs = $shop->getShopBaseInfo($field,$arg);
        $rs['brand_type'] = explode(',',$rs['brand_type']);
        return $rs;
    }

    /**
     * 修改门店logo
     * @desc 根据sid,uid,修改门店logo
     * @return boolean true表示成功/false表示成功
     */
    public function updateLogo()
    {
        $shop = new Domain_HomePageShop();

        $dst = DI()->helper->getFileName('uploads', $this->logo); //获取logo的上传路径

        $logo = DI()->cosSdk->uploadCos('bikeshop', $this->logo['tmp_name'], $dst);

        $logoScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $logo['data']['source_url']);

        $rs = $shop->updateLogo($this->id, $this->uid, $logoScore);

        return $rs ? true : false;
    }

    /**
     * 删除门店
     * @desc 删除门店
     * @return boolean id true表示成功,false表示失败
     */
    public function delete()
    {
        //todo 判断是否是当前用户的门店
        $shop = new Domain_HomePageShop();

        return $shop->delete($this->id);
    }

    /**
     * 门店列表
     * @desc 根据uid获取门店列表
     * @return int id id
     * @return int gid 上架的商品id
     * @return string title 标题
     * @return string address 地址
     * @return string logo logo
     */
    public function shopList()
    {
        $shop = new Domain_HomePageShop();
        $field = 'id,gid,title,address,logo';

        //先读取是否有相同
        $details = $shop->getUidList($this->uid, $field);

        for ($i = 0; $i < count($details); $i++) {
            $details[$i]['gid'] = json_decode($details[$i]['gid']);
        }

        return $details;
    }

    /**
     * 门店详情
     * @desc 获取所有门店详情
     * @return array details 详情数据
     * @return array images 轮播图片
     */
    public function details()
    {
        $shop = new Domain_HomePageShop();
        $details = $shop->shop();

        $details['0']['image_url'] = json_decode($details['0']['image_url']);

        return [
            'details' => $details,
        ];
    }

    /**
     * 门店评价
     * @desc 门店评价
     * @return int id 1表示添加成功.0表示失败
     */
    public function review()
    {
        $shop = new Domain_ShopReview();
        //todo 判断用户是否认证,未认证跳转到认证界面
        $data = [
            'uid' => $this->uid,
            'sid' => $this->sid,
            'message' => $this->message,
            'score' => $this->score,
            'time' => date('Y-m-d H:i:s')
        ];

        $rs = $shop->add($data);

        return $rs ? true : false;
    }

    /**
     * 商品评价列表
     * @desc 根据sid获取评价列表,没有返回空
     * @return string id 评价id
     * @return string message 评价信息
     * @return string score 评分
     * @return string time 评价时间
     * @return string username 用户名
     */
    public function reviewList()
    {
        $domain = new Domain_ShopReview();

        return $domain->getList($this->sid);
    }

    /**
     * 门店详情
     * @desc 根据sid,获取门店详细数据
     * @return array banner banner头
     * @return string banner.title 商品id
     * @return string banner.image_url 商品标题
     * @return string banner.tel 类型
     * @return string banner.address 定金
     * @return string banner.star 零售价
     * @return array selectList 门店商品列表
     * @return string selectList.id 商品id
     * @return string selectList.title 标题
     * @return string selectList.brand 品牌
     * @return string selectList.type 类型
     * @return string selectList.earnest 定金
     * @return string selectList.retail_price 零售价
     * @return string selectList.image_url 图片链接
     */
    public function shopDetails()
    {
        $shop = new Domain_HomePageShop();
        $products = new Domain_HomePageProducts();
        $category = new Model_Category();

        //根据门店的id,获取门店banner信息
        $field2 = 'title,image_url,tel,address,star';

        //根据门店的id,获取门店信息
        $gid = $shop->getGid($this->sid);

        if (!$gid) {
            return '门店不存在';
        }

        $field = 'id,title,brand,type,earnest,retail_price,image_url';
        $b = explode(',', json_decode($gid['gid']));

        //根据gid获取商品数据
        $data = $products->getByGid($b, $field, 10);
        if (!empty($data[0])) {
            foreach ($data as $k => $v) {
                $v['type'];
                $arg = [
                    'id' => $v['type']
                ];
                $type[] = $category->getDataByField('category', $arg);
                $data[$k]['type'] = $type[$k]['category'];
            }

            return [
                'banner' => $shop->getBanner($this->sid, $field2),
                'selectList' => $data,
            ];
        } else {
            foreach ($data as $k => $v) {
                $v['type'];
                $arg = [
                    'id' => $v['type']];
                $type[] = $category->getDataByField('category', $arg);
                $data[$k]['type'] = $type[$k]['category'];
            }

            return [
                'banner' => $shop->getBanner($this->sid, $field2),
                'selectList' => [],
            ];
        }
    }

    /**
     * 地图选择列表
     * @desc 根据条件获取列表
     * @return string title 标题
     * @return string shop_type 商品类型
     * @return string earnest 定金
     * @return string retail_price 零售价
     * @return string image_url 图片地址
     * @return boolean false 搜索不到数据
     * @throws -1504 参数不正确,查询不到数据
     */
    public function screening()
    {
        $shop_type = new Domain_HomePageShop();
        $size = explode('-', $this->store_size);
        $size[1] = substr($size[1],0,3);
        $field = 'id,title,address,tel,store_size,image_url,longitude,latitude';
        $arr = [];
        //如果等于0,就删除变量
        if ($this->shop_type == '') {
            unset($this->shop_type);
        }
        if ($this->brand_type == '') {
            unset($this->brand_type);
        }
        if ($this->store_size == '') {
            unset($this->store_size);
        }

        if (!empty($this->shop_type)) {
            if (!empty($this->brand_type)) {
                if (!empty($this->store_size)) {
                    $data = [
                        'shop_type' => $this->shop_type,
                        'brand_type' => $this->brand_type,
                        'store_size >= ?' => $size[0],
                        'store_size <= ?' => $size[1]
                    ];
                } else {
                    //store_size 为空的时候
                    $data = [
                        'shop_type' => $this->shop_type,
                        'brand_type' => $this->brand_type,
                    ];
                }
            } else {
                //brand_type 为空的时候
                if (!empty($this->store_size)) {
                    $data = [
                        'shop_type' => $this->shop_type,
                        'store_size >= ?' => $size[0],
                        'store_size <= ?' => $size[1]
                    ];
                } else {
                    //brand_type store_size 为空的时候
                    $data = [
                        'shop_type' => $this->shop_type
                    ];
                }
            }
        } else {
            //shop_type为空的时候
            if (!empty($this->brand_type)) {
                if (!empty($this->store_size)) {
                    $data = [
                        'brand_type' => $this->brand_type,
                        'store_size >= ?' => $size[0],
                        'store_size <= ?' => $size[1]
                    ];
                } else {
                    $data = [
                        'brand_type' => $this->brand_type
                    ];
                }
            } else {
                //shop_type  brand_type 为空的时候
                if (!empty($this->store_size)) {
                    $data = [
                        'store_size >= ?' => $size[0],
                        'store_size <= ?' => $size[1]
                    ];
                } else {
                    //shop_type  brand_type store_size 为空的时候
                    return $shop_type->getDefaultList();
                }
            }
        }

        if (empty($shop_type->screening($data))) {
            return $arr;
        }

        return @$shop_type->screening($data);
    }

    /**
     * 门店上架数据
     * @desc 获取门店上架数据
     * @return string title 标题
     * @return string brand 品牌
     * @return string type 类型
     * @return string earnest 定金
     * @return string retail_price 零售价
     * @return string image_url 图片地址
     */
    public function shelves()
    {
        //todo 根据用户id+门店id  查询上架的数据
        $shop = new Domain_HomePageShop();
        $products = new Domain_HomePageProducts();

        //根据商家id读取gid
        $jsonGid = $shop->getGid($this->id);

        //根据gid查询数据
        $field = 'id,title,brand,type,earnest,retail_price,image_url';

        $gid = json_decode($jsonGid['gid']);

        $data = explode(',', $gid);  //分割gid的值

        $rs = $products->getByGid($data, $field, count($data));

        return $rs;
    }

    /**
     * 地图选车筛选列表
     * @desc 地图选车筛选列表
     * @return int $category[id] 门店id
     * @return string  $category[category] 分类名
     * @return int $brand[id] 品牌id
     * @return string $brand[brand] 品牌名
     * @return int $range[id] 区间规模id
     * @return string $range[price_range] 规模区间
     */
    public function getSelect()
    {
        $type = new Domain_Type();
        $brand = new Domain_Brand();
        $store = new Domain_Store();

        $a = array_column($brand->getList(), 'brand');
        $a[] = '所有品牌';

        $b = array_column($type->getList(), 'shop_type');
        $b[] = '所有类型';

        $c = array_column($store->getList(), 'store_size');
        $c[] = '所有大小';

        return [
            'shop_type' => $b,
            'brands' => $a,
            'store_size' => $c,
        ];


    }

    /**
     * 根据商品id获取门店列表
     * @desc 根据商品id获取门店列表
     * @return int id 门店id
     * @return int star 星级
     * @return string title 门店名称
     * @return string address 门店地址
     * @return string latitude 经度
     * @return string longitude 纬度
     * @return string tel 电话号码
     * @return string distance 距离
     */
    public function getGidShopList()
    {
        //获取商品id,列出存在的门店
        $shop = new Domain_HomePageShop();
        $products = new Domain_HomePageProducts();
        $field = 'id,star,title,address,latitude,longitude,tel,image_url';

        //获取到所有的gid列表
        $list = $shop->getIDGid();
        $rs = [];

        foreach ($list as $k => $v) {
            $gid_arr = explode(',', json_decode($v['gid']));
            if (in_array($this->gid, $gid_arr)) {
                $rs[] = $v['id'];   //返回上架id
            }
        }
        $dataList = $shop->getIdList($rs, $field);  //循环上架id,分别读取出列表数据
        foreach ($dataList as $k => $v) {
            //读取数据库中的经纬度
            $distance[] = $shop->distance($this->longitude, $this->latitude, $v['longitude'], $v['latitude']);
            $longitude = $v['longitude'];
            $latitude = $v['latitude'];

            //循环根据用户的经纬度                SELECT slc(1,1,2,2) from DUAL
//            return "slc('$this->longitude','$this->latitude','$longitude','$latitude')";
            $reslut[] = $distance[$k][0]["slc('$this->longitude','$this->latitude','$longitude','$latitude')"];
//            $reslut[] = $distance[$k][0]["slc('120.184626','30.301459','120.187446','30.268562')"];

            $distanceData[] = substr($reslut[$k], 0, 3) . '千米';//算出距离返回
            foreach ($distanceData as $k => $v) {
                $dataList[$k]['distance'] = $v;
            }
        }
//        return $dataList;

        foreach($dataList as $k2=>$v2){
            $arg = [
                'sid' => $v2['id'],
                'gid' => $this->gid,
            ];
            $number =  $products->getByIdSidNatureVal('number',$arg);
//            return $number;
            $number = array_filter(array_column($number,'number'));
            if(!$number){
                unset($dataList[$k2]);
            }
        }


        return $dataList;
    }

    /**
     * 根据分类后的门店筛选
     * @desc 根据分类后的门店筛选
     * @return int id 商品id
     * @return int star 星级
     * @return string title 门店标题
     * @return string address 门店地址
     * @return int tel 门店号码
     * @throws -1204 参数不正确,查询不到数据true,false
     */
    public function getGidScreening()
    {
        $shop = new Domain_HomePageShop();

        $dateList = $this->getGidShopList();
//        return $dateList;

//        foreach ($dateList as $k => $v) {
//            //读取数据库中的经纬度
//            $distance[] = $shop->distance($this->longitude, $this->latitude, $v['longitude'], $v['latitude']);
//            $longitude = $v['longitude'];
//            $latitude = $v['latitude'];
//
//            //循环根据用户的经纬度                SELECT slc(1,1,2,2) from DUAL
////            return "slc('$this->longitude','$this->latitude','$longitude','$latitude')";
//            $reslut[] = $distance[$k][0]["slc('$this->longitude','$this->latitude','$longitude','$latitude')"];
////            $reslut[] = $distance[$k][0]["slc('120.184626','30.301459','120.187446','30.268562')"];
//
//            $distanceData[] = substr($reslut[$k], 0, 3) . '千米';//算出距离返回
//            foreach ($distanceData as $k => $v) {
//                $dataList[$k]['distance'] = $v;
//            }
//
//            //星级统计
//            $starData[] = $dateList[$k]['star'];
//
//        }
//        return $dateList;


        //默认
        if(!$this->distance && !$this->star){
            return $dateList;
        }

        //按照星级升序
        if (strtoupper($this->star) == 'DESC') {
            array_multisort($dateList, SORT_DESC);
            return $dateList;
        }

        //按照星级降
        if (strtoupper($this->star) == 'ASC') {
            array_multisort($dateList, SORT_ASC);
            return $dateList;
        }

        //按照距离升序
        if (strtoupper($this->distance) == 'DESC') {
            array_multisort($dateList, SORT_ASC);
            return $dateList;
        }

        //按照距离降序
        if (strtoupper($this->distance) == 'ASC') {
            array_multisort($dateList, SORT_DESC );
            return $dateList;
        }


    }

    /**
     * 获取星级,距离分类列表
     * @desc 获取星级,距离分类列表
     * @return string stars 星级
     * @return string stars 距离
     */
    public function getGidSelect()
    {
        $star = new Domain_Star();
        $distance = new Domain_Distance();

        return [
            'stars' => array_column($star->getList(), 'star'),
            'distances' => array_column($distance->getList(), 'distance'),
        ];
    }

    /**
     * 获取门店基本信息
     * @desc 根据门店id,用户id,门店基本信息
     * @return string logo logo图
     * @return string title 门店标题
     * @return string tel 联系电话
     * @return string address 门店地址
     * @return string shop_time 门店营业时间
     */
    public function getShopBaseInfo()
    {
        $shop = new Domain_HomePageShop();
        $field = 'logo,title,tel,address,shop_time';
        $arg = [
            'id' => $this->id,
            'uid' => $this->uid
        ];

        return $shop->getShopBaseInfo($field,$arg);
    }

    /**
     * 门店上架商品筛选
     * @return array|string
     */
    public function shopDataScreening()
    {
        $shop = new Domain_HomePageShop();
        $products = new Domain_HomePageProducts();
        $category = new Domain_Category();
        if ($this->retail_price == '高到低') {
            $this->retail_price = 'DESC';
        }
        if ($this->retail_price == '低到高') {
            $this->retail_price = 'ASC';
        }
        if ($this->category == '所有类型' || $this->brand == '所有品牌' || $this->retail_price == '所有价格') {
            $this->category = null;
            $this->brand = null;
            $this->retail_price = null;
        }
        $newCategory = '';
        if ($this->category) {
            $arg = [
                'category' => $this->category
            ];
            $newCategory = $category->getDataByField('id', $arg);
        }

        $data = [
            'category' => $newCategory ? $newCategory['id'] : null,
            'brand' => $this->brand,
            'retail_price' => $this->retail_price,
            'page' => $this->page,
        ];
        $arr = [];
        if (!$this->sid) {
            return '门店id不允许为空';
        }

        //根据门店的id,获取门店信息
        $gid = $shop->getGid($this->sid);
        if (!$gid) {
            return $arr;
        }
        $field = 'id,category,title,brand,type,earnest,retail_price,image_url';
        $explodeData = explode(',', json_decode($gid['gid']));
        $productData = $products->getByGid($explodeData, $field, count($explodeData)); //查询门店数据
        $filter = array_filter($data);
        $count = count($filter);
        $arrValue = array_values($filter);
//        return $arrValue;
        //参数不固定,传参不一,暂时屏蔽方法
        @$resultData = DI()->helper->shopDataScreening($count, $productData, $arrValue[0], $arrValue[1], $arrValue[2]);

//        return $resultData;

        if ($count == 1) {
            return $products->getByGid($explodeData, $field, $this->page * 10);
        }

        if ($resultData) {
            foreach ($resultData as $k2 => $v2) {
                $arg = [
                    'id' => $v2['type']
                ];

                $type2[] = $category->getDataByField('category', $arg);
                $resultData[$k2]['type'] = $type2[$k2]['category'];
                unset($resultData[$k2]['category']);
                return $resultData;
            }
        } else {
            return $arr;
        }
        //所有属性,返回门店的数据
    }

    /**
     * 添加门店选择的商品品牌
     * @desc 添加门店选择的商品品牌
     * @return int first 商品开头字母
     * @return array info 商品数据
     * @return string info[key][name] 商品名称
     * @return string info[key][logo] 商品logo
     */
    public function brand()
    {
        $banner = new Domain_Brand();

        $bannerData = $banner->getBrand('brand,logo', [
            'is_show' => 1
        ]);
//        return $bannerData;
//        $userName = array('啊啊啊', '美利达', '王五', '小二', '猫蛋', '狗蛋', '王花', '三毛', '小明', 'Mary', '李刚', '张飞', 'Lucy');
        $userName = array_column($bannerData, 'brand');
        sort($userName);
        $charArray = [];
        foreach ($userName as $name) {
            $char = mb_substr($name, 0, 1, 'utf-8');
            $char2 = strtoupper(substr(DI()->py->get($char), 0, 1));
            DI()->logger->debug('char',$char2);
            $nameArray = array();//将姓名按照姓的首字母与相对的首字母键进行配对
            if (count($charArray[$char2]) != 0) {
                $nameArray = $charArray[$char2];
            }
            array_push($nameArray, $name);
            $charArray[$char2] = $nameArray;
        }

        //循环品牌数据
        foreach($bannerData as $k2=>$v2){
            //循环合并后的数据
            foreach($charArray as $k3=>$v3){
                foreach($v3 as $k4=>$v4){
                    //判断品牌名是否相同，相同则加入同一数组中
                    if($v4 == $v2['brand']){
                        $a[$k3]['first'] = $k3;
                        $a[$k3]['info'][] = [
                            'name' => $v2['brand'],
                            'logo' => $v2['logo']
                        ];
                    }
                }
            }
        }
        ksort($a);
        return array_values($a);
    }
}































































