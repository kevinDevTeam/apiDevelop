<?php 

/**
 * 特殊商品（2.0） -
 * Date: 2017/2/13
 * Time: 10:00
 */
class Api_SpecialGood extends PhalApi_Api{
      public function getRules()
       {
         return [
           'release'=>[
               'title'=>['name'=>'tile','type'=>'string','require'=>true,'desc'=>'商品标题'],
               'shop'=>['name'=>'shop','type'=>'string','require'=>true,'desc'=>'店铺ID'],
               'sign' =>['name'=>'sign','type'=>'string','require'=>true,'desc'=>'商品标签'],
               'price'=>['name'=>'price','type'=>'string','require'=>true,'desc'=>'价格（起拍价）'],
               'cost_price'=>['name'=>'cost_price','type'=>'string','require'=>true,'desc'=>'原价(保证金)'],
               'sort'=>['name'=>'sort','type'=>'string','require'=>true,'desc'=>'商品分类'],
               'brand'=>['name'=>'brand','type'=>'string','require'=>true,'desc'=>'商品品牌'],
               'details'=>['name'=>'details','type'=>'string','require'=>true,'desc'=>'商品介绍'],
               'start_time'=>['name'=>'start_time','type'=>'string','desc'=>'起拍时间'],
               'end_time'=>['name'=>'end_time','type'=>'string','desc'=>'结束时间'],
               'fare'=>['name'=>'fare','type'=>'string','desc'=>'每次加价'],
               'file'=>[
                   'name' => 'file',  'min' => 0, 'max' => 1024 * 1024, 'range' => array('image/jpg', 'image/jpeg', 'image/png'), 
                    'ext' => array('jpg', 'jpeg', 'png'), 'desc'=>'商品图片'      
                      ],
             ],
           'goodList'=>[
               'shop'=>['name'=>'shop','type'=>'string','require'=>true,'desc'=>'店铺ID'],
             ],
           'goodDetail'=>[
                'gid'=>['name'=>'gid','type'=>'string','require'=>true,'desc'=>'商品ID'],
           ],
           'addNum'  =>[
               'gid'=>['name'=>'gid','type'=>'string','require'=>true,'desc'=>'商品ID'],
            ],

         ];
       }
   /**
   *商品发布
   *@desc商品发布
   * @return boolean add true表示成功/false表示失败
   */
   public function release(){
      
     $domain=new Domain_SpecialGood();
      $file=$_FILES['file'];
        $name=array();
        $i=0;
       foreach ($file['tmp_name'] as $k) {
          $path['type']=$file['type'][$i]; 
          $dst = DI()->helper->getFileName('good/'.date("Ymd"),  $path); //获取logo的上传路径      
          $file1 = DI()->cosSdk->uploadCos('bikeshop', $k, $dst);
          $fileScore = str_replace("bikeshop-1251022106.costj.myqcloud.com", "image.olbike.com", $file1['data']['source_url']);
          $name[]= $fileScore;
        }  
     $data=array(
           'title'    =>$this->title,
           'shop'     =>$this->shop,  
           'image_url'=>$fileScore,
           'images'   =>json_encode($name),
           'sign'     =>$this->sign,
           'price'    =>$this->price,
           'cost_price'  =>$this->cost_price,
           'sort'     =>$this->sort,
           'brand'    =>$this->brand,
           'details'  =>$this->details,
      	);
     if($this->sign=='拍卖'){
     	$data['start_time']=$this->start_time;
     	$data['end_time']=$this->end_time;
     	$data['fare']=$this->fare;
     }
     return $domain->add($data) ?  true : false;
   } 
  /**
   *商品修改
   *@desc商品编辑
   * @return boolean add true表示成功/false表示失败
   */
  public function  upGood(){
      
      $domain=new Domain_SpecialGood();
      $id=$this->gid;
      $data=array(



      	);
   return $domain->updated($id,$data) ?  true : false; 
   }   
  /**
   *商品删除
   *@desc商品删除
   * @return boolean add true表示成功/false表示失败
   */
   public function deleted(){
     $domain=new Domain_SpecialGood(); 
     return $domain->deleted($this->gid) ?  true : false; 
   
   }
  /**
   *商品列表
   *@desc商品列表
   *@return string sign 商品标签
   *@return string title 商品标题
   *@return string price 售价
   *@return string cost_price 原价（拍卖的是保证金）
   *@return string image_url 商品图片
   */
  public  function goodList(){
     $domain=new Domain_SpecialGood(); 
     $re=$domain->getShopList($this->shop);
     return $re;
   }
  /**
   *商品详情
   *@desc 商品详情
   * @return string id  商品id
   * @return object shop 店铺信息对象
   * @return string title 用户名字
   * @return string images 用户名字
   * @return string sign  标签
   * @return string price 售价(起拍价)
   * @return string cost_price 原价(保证金)
   * @return string sort  类型
   * @return string brand 品牌
   * @return string fare 最低加价额度
   * @return string start_time 开始时间
   * @return string end_time 结束时间
   * @return string details 商品简介
   * @return string number 拍卖围观人数
   * @return string enrollment 报名人数
   * @return string now_time 当前系统时间
   */ 
   public  function  goodDetail(){
     
     $domain=new Domain_SpecialGood(); 
     $re=$domain->getAll($this->gid,'*');
     $shop=$domain->getName($re['shop']);
      if($re['sign']=='拍卖'){
          $num=$domain->getBao($this->gid);
          $data=$re;
          $date['now_time']=date("Y-m-d H:i:s");
          $data['enrollment']=$num;
      }else{
     $data=array(
           'title'    =>$re['title'],
           'images'   =>$re['images'],
           'sign'     =>$re['sign'],
           'price'    =>$re['price' ],
           'cost_price'  =>$re[ 'cost_price'],
           'sort'     =>$re[ 'sort'],
           'brand'    =>$re['brand'],
           'details'  =>$re['details'],
     	);
     }
      $data['shop']=$shop;
     return $data;    
   }
  /**
   *拍卖商品围观次数
   *@desc 增加围观次数
   *@return boolean addNum true表示成功/false表示失败
   */
  public function addNum(){

     $domain=new Domain_SpecialGood();  
     $num=$domain->getNum($this->gid);
     $data=array(
       'number'=>$num['number']+1,
      );
     return $domain->update($this->gid,$data) ?  true : false;  
   }

}