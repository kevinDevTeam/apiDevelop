<?php

/**
 * 单车易购统计
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/24
 * Time: 10:47
 */
class  Api_DataStatistics extends PhalApi_Api{
    
   

   /**
   *数据统计
   *@desc 单车易购数据统计
   *
   */
   public function Statistics(){
      header("Access-Control-Allow-Origin:*");
     /*星号表示所有的域都可以接受，*/
      header("Access-Control-Allow-Methods:GET,POST");
      $domin=new Domain_DataStatistics();
      $data=array(
        'user' =>$domin->countUser(),
        'good' =>$domin->countGood(),       
        'order'=>$domin->countOrder(),  
        'shop' =>$domin->getShop(),
        'brand'=>$domin->getList(),
      	);
    
    return $data;
   }

}






?>