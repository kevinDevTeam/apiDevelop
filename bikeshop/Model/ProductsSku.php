<?php

class Model_ProductsSku extends PhalApi_Model_NotORM {

    //根据商品id 查询部分数据
    public function getByIdValue($id){
        return $this->getORM()
            ->select('`values`')
            ->where('g_id',$id)
            ->fetchAll();
//        $sql = 'SELECT `values` FROM zixc_bikeshop_good_sku WHERE (g_id = :id)';
//
//        $param =[
//            ':id'=>$id,
//        ];
//
//        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

   //根据id 查询销售属性组合
    public function getNature($id){
         return $this->getORM()
                  ->select('`retail_price`,`images`,`values`')
                  ->where('id',$id)
                  ->fetch();
    }

    public function getDataByFieldSku($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetch();
    }




    protected function getTableName($id) {
        return 'zixc_bikeshop_good_sku';
    }

}