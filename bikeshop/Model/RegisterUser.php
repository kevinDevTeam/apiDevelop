<?php
 /*
 */

class Model_RegisterUser extends PhalApi_Model_NotORM {


    public function getUser ($key,$value) {
        return $this->getORM()
            ->select($key)
            ->where($key,$value)
            ->fetch();
    }

  public function getPwd($phone){
    return $this->getORM()
             ->select('password,username,email')
             ->where('phone',$phone)
             ->fetch();
    } 

   public function  inUser($data){
      $user= DI()->notorm->zixc_bikeshop_user;
      $user->insert($data);
       $id = $user->insert_id();
      return $id; 
   }

   //完善用户信息
   public function perfectInfo($uid,$data){
      return $this->getORM()
                  ->where('uid',$uid)
                  ->update($data);
   }

  public  function upPwd($phone,$password){
     $user= DI()->notorm->zixc_bikeshop_user;  
    return  $user->where('phone',$phone)->update(['password'=>$password]);
    
  }
   //插入手机号码验证码
  public function inCode($data){
     
    $user = DI()->notorm->zixc_bikeshop_code;
   
    $user->insert($data);

    $id = $user->insert_id();

    return $id; 
  }
 // 验证验证码
  public function VerificationCode($phone,$code){
     
    $user = DI()->notorm->zixc_bikeshop_code;
   
    $code=$user->select('time')
           ->where('phone',$phone)
           ->and('code',$code)
           ->fetch();   
     DI()->notorm->zixc_bikeshop_code->where('phone',$phone)->delete();   
     return $code;
  }
  
    protected function getTableName($id) {
        
      return 'zixc_bikeshop_user';
    }

}
