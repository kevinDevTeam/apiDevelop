<?php

class Model_Category extends PhalApi_Model_NotORM
{

    //获取列表
    public function getList()
    {
        return $this->getORM()
            ->select('category')
            ->where('parent_id',0)
            ->fetchAll();
    }

    //根据category查询id
    public function getDataByField($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetch();
    }


    protected function getTableName($id)
    {
        return 'zixc_bikeshop_goods_category';
    }
}
