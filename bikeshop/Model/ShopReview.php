<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/9
 * Time: 9:46
 */

class Model_ShopReview extends PhalApi_Model_NotORM {

    public function add($data){
        return $this->getORM()
            ->insert($data);
    }


    public function getList($sid){
        $sql = "SELECT
	a.id,a.message,a.score,a.time, b.username
FROM
	zixc_bikeshop_shop_review AS a
JOIN zixc_bikeshop_user AS b ON a.uid = b.uid
AND sid = :sid";

        $param = [
            ':sid' => $sid
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }






    protected function getTableName($id) {

        return 'zixc_bikeshop_shop_review';

    }



}