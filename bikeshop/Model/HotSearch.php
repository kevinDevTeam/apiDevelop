<?php

class Model_HotSearch extends PhalApi_Model_NotORM {

    public function getDataByField($field,$arg) {
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchAll();
    }

    protected function getTableName($id) {
        return 'zixc_bikeshop_hot_search';
    }

}