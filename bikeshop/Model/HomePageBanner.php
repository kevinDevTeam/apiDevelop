<?php

class Model_HomePageBanner extends PhalApi_Model_NotORM {

    public function getBanner() {
        return $this->getORM()
            ->select('*')
            ->fetchAll();
    }


    protected function getTableName($id) {
        return 'zixc_bikeshop_ad';
    }
}
