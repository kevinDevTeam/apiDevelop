<?php

/**
* 拍卖订单
*2017.2.14
*/

class Model_AuctionOrder extends PhalApi_Model_NotORM {
    
    /*
    *生成订单
    */
	public function add($data){
      
              $this->getORM()
                   ->insert($data);
         return $this->getORM()->insert_id();      
     	}
	//出价+加价
     public function updated($gid,$uid,$data){
        
         return $this->getORM()
                     ->where('uid',$uid)
                     ->and('g_id',$gid)
                     ->updated($data);
       } 	
    //查询价格
    public function getMoney($gid,$uid){
        
         return $this->getORM()
                     ->select('money')
                     ->where('uid',$uid)
                     ->and('g_id',$gid)
                     ->fetch();
       }    
    //查询最高竞价
    public function getMax($gid){
        
        return $this->getORM()
                     ->where('g_id',$gid)
                     ->max('money'); 	
    } 
   //查询我的拍卖
  public function getPai($uid,$status){
    
     return $this->getORM()
                 ->select('o_id,g_id,status')
                 ->where('uid',$uid)
                 ->and('status',$status)
                 ->fetchAll();
  }
 //根据gid查询出价列表
  public function  offerList($gid){
    $sql = '
        SELECT 
         ep.money,
         ep.add_time,
         u.username
        FROM 
         zixc_bikeshop_auction AS ep,
         zixc_bikeshop_user AS u
        WHERE 
         ep.uid = u.uid
       AND 
        g_id =:g_id
        ';

   $params = array(':g_id' => $gid);
  
   return DI()->notorm->example->queryAll($sql, $params);   
  }
  //根据gid查询报名人数
  public  function getNum($gid){
      
      return $this->getORM()
                  ->where('g_id',$gid)
                  ->count();

  }
  protected function getTableName($id) {
        
      return 'zixc_bikeshop_auction';
    
    }

}