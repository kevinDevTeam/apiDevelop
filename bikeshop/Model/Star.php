<?php

class Model_Star extends PhalApi_Model_NotORM {

    public function getList() {
        return $this->getORM()
            ->select('star')
            ->limit(0,4)
            ->fetchAll();
    }



    protected function getTableName($id) {
        return 'zixc_bikeshop_shop_star';
    }

}