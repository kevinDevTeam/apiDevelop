<?php

/*
*查询用户信息
*/

class Model_Browse extends PhalApi_Model_NotORM
{
    public function getBrowse($uid, $start, $end)
    {

        return $this->getORM()
            ->select('time,zixc_bikeshop_goods.title,zixc_bikeshop_goods.retail_price,zixc_bikeshop_goods.id,zixc_bikeshop_goods.image_url')
            ->where('uid', $uid)->limit($start, $end)->order('zixc_bikeshop_browse.id DESC')
            ->fetchAll();

    }

    public function  inBrowse($data)
    {
        $user = DI()->notorm->zixc_bikeshop_browse;
        $user->insert($data);
        $id = $user->insert_id();
        return $id;
    }

    public function detele($uid)
    {
        return $this->getORM()->where('uid', $uid)->delete();
    }

    public function getFieldByData($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchOne();
    }

    public function getFieldEdit($arg,$data){
        return $this->getORM()
            ->where($arg)
            ->update($data);
    }

    protected function getTableName($id)
    {

        return 'zixc_bikeshop_browse';

    }

}
