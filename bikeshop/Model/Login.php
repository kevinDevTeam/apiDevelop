<?php
 /*
 *查询用户信息11
 */

class Model_Login extends PhalApi_Model_NotORM {


    public function getUser ($username) {
        return $this->getORM()
            ->select('password,uid')
            ->or('phone',$username)->or('email',$username)->or('username',$username)
            ->fetch();
    }

    public function   getAppkey($appkey){
       return $this->getORM()
            ->select('uid,username')
            ->where('appkey',$appkey)
            ->fetch();
    }
    
   public function  inUser($data){
      $user= DI()->notorm->zixc_bikeshop_user;
      $user->insert($data);
       $id = $user->insert_id();
      return $id;
   }

  public function getNum($uid){
    $user= DI()->notorm->zixc_bikeshop_shopcart;
    return $user->where('uid',$uid)->and("isDisplay",1)->sum('number');
  }

  //查询是否授权登录
   public function checkWei($unionid){
    
    $user= DI()->notorm->zixc_bikeshop_wei;

    return $user->select('uid')->where('unionid',$unionid)->fetch();

   }
  //查询QQ是否绑定用户
   public  function checkQq($unionid){

     $user= DI()->notormWei->pre_common_member_connect;

     return $user->select('uid')->where('conopenid',$unionid)->fetch();
   }
  //插入微信登录信息
   public function inWei($data){

      $user= DI()->notorm->zixc_bikeshop_wei;

       $user->insert($data);

       $id = $user->insert_id();
  
       return $id;
   }

  //查询论坛表是否微信绑定
   public function getWei($unionid){

    $user= DI()->notormWei->zixc_wq_login_member;

    return $user->select('uid')->where('unionid',$unionid)->fetch();
  
  } 

    protected function getTableName($id) {
        
      return 'zixc_bikeshop_user';
    
    }

}
