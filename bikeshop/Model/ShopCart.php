<?php
/*
*/

class Model_ShopCart extends PhalApi_Model_NotORM {

   public function shopCart($data){
       return $this->getORM()
           ->insert($data);
   }

    //根据商品id查询数据
    public function getByGid($uid,$gid){
        return $this->getORM()
            ->select('
            zixc_bikeshop_shopcart.id,
            zixc_bikeshop_shopcart.sid,
            zixc_bikeshop_shopcart.number,
            zixc_bikeshop_shopcart.zixc_bikeshop_goods_id,
            zixc_bikeshop_shopcart.price,
            zixc_bikeshop_goods.category,
            zixc_bikeshop_goods.title,
            zixc_bikeshop_goods.earnest,
            zixc_bikeshop_goods.image_url
            ')
            ->where([
                'zixc_bikeshop_goods_id' => $gid,
                'zixc_bikeshop_shopcart.uid' => $uid,
                'isDisplay' => 0
            ])
            ->fetchAll();
    }


    //根据id uid 查询数据
    public function getByIdUid($field,$data){
       return $this->getORM()
           ->select($field)
           ->where($data)
           ->fetchAll();
    }

    //根据uid获取数据
    public function getList($uid)
    {
        return $this->getORM()
            ->select('
            zixc_bikeshop_shopcart.id,
            zixc_bikeshop_shopcart.sid,
            zixc_bikeshop_shopcart.uid,
            zixc_bikeshop_shopcart.number,
            zixc_bikeshop_shopcart.zixc_bikeshop_goods_id,
            zixc_bikeshop_shopcart.nature_val,
            zixc_bikeshop_shopcart.price,
            zixc_bikeshop_goods.title,
            zixc_bikeshop_goods.earnest,
            zixc_bikeshop_goods.image_url
            ')
            ->where([
                'zixc_bikeshop_shopcart.uid' => $uid,
                'isDisplay' => 1
            ])
            ->fetchAll();
//        return $this->getORM()
//            ->select('*')
//            ->where('uid', $uid)
//            ->fetchAll();
    }

    public function updateNumber($field,$number){

        return $this->getORM()
            ->where($field)
            ->update($number);
    }

    public function updateData($id,$data){
        return $this->getORM()
            ->where([
                'id'=>$id,
            ])
            ->update($data);
    }

  public function upDisplay($id,$data){
     return $this->getORM()
            ->where('id',$id)
            ->update($data);
    }
   //根据购物车ID 查询购物车
    public function getall($id){
        return $this->getORM()
                  ->select('zixc_bikeshop_goods.earnest,zixc_bikeshop_goods.image_url,zixc_bikeshop_goods.title,zixc_bikeshop_goods.retail_price,sid,nature_val,number,price')
                  ->where('zixc_bikeshop_shopcart.id',$id)
                  ->fetch();
    }

  //根据购物车ID 查询购物车
    public function getCart($id){
        return $this->getORM()
                  ->select('sid')
                  ->where('id',$id)
                  ->fetch();
    }
 //根据购物车sID 查询购物车
    public function getCartId($sid){
        return $this->getORM()
                  ->select('id')
                  ->where('sid',$sid)
                  ->fetchAll();
    }

    //根据id 删除购物车
    public function deleteData($id){
        return $this->getORM()
            ->where('id',$id)
            ->delete();
    }

    //统计购物车总数
    public function countShopCart($uid){
        $sql = "select SUM(number) from zixc_bikeshop_shopcart  where uid=:uid AND isDisplay=1";

        $param =[
            ':uid'=>$uid
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    //根据uid获取 nature_val 值
    public function getByUidValues($uid){
        return $this->getORM()
            ->select('zixc_bikeshop_goods_id','nature_val','id')
            ->where('uid',$uid)
            ->fetchAll();
    }

    //根据属性值,查询id
    public function getNatureById($nature){
        $sql = "select `id` from zixc_bikeshop_shopcart  where nature_val=:nature_val";

        $param =[
            ':nature_val'=>$nature
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    //根据id查询nature_val值
    public function getByIdNature($id){
        return $this->getORM()
            ->select('sid,zixc_bikeshop_goods_id,number,nature_val')
            ->where('id',$id)
            ->fetchOne();
    }

    public function getBYUidIdSId($id,$uid){
////        $a = 43,44;
        foreach ($id as $key => $value) {
            foreach ($id[$key] as $k2=> $v2) {
                $v3[] = $v2;
            }
        }

//        $b = implode(',',$v2);

//        return $id;
        $sql = "SELECT
                    a.id,
                    a.sid,
                    a.uid,
                    a.nature_val,
                    a.zixc_bikeshop_goods_id,
                    a.number,
                    a.price,
                    a.date,
                    b.earnest,
                    b.title,
                    b.image_url
                FROM
                    zixc_bikeshop_shopcart AS a
                JOIN zixc_bikeshop_goods AS b ON a.zixc_bikeshop_goods_id = b.id
                WHERE
                    a.id = :id1 or a.id=:id2 or a.id=:id3 or a.id=:id4 or a.id=:id5 or a.id=:id6 or a.id=:id7
                AND uid =:uid";

        $param =[
            ':id1'=>@$v3[0],
            ':id2'=>@$v3[1],
            ':id3'=>@$v3[2],
            ':id4'=>@$v3[3],
            ':id5'=>@$v3[4],
            ':id6'=>@$v3[5],
            ':id7'=>@$v3[6],
            ':uid'=>$uid
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);

//        return $this->getORM()
//            ->select('*')
//            ->where([
//                'id' => $id,
//                'sid' => $sid,
//                'uid' => $uid
//            ])
//            ->fetchAll();
    }

    public function getBySid($sid){
        return $this->getORM()
            ->select('*')
            ->where([
                'id' => $sid,
            ])
            ->fetchAll();
    }





    protected function getTableName($id) {

        return 'zixc_bikeshop_shopcart';

    }


}
