<?php

/**
 * 签到管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:02
 */
class Api_Checkin extends PhalApi_Api
{
    public function getRules()
    {
        return [
            'total' => [
                'type' => ['name' => 'type', 'type' => 'string', 'require' => true,'desc' => '请求名称'],
                'page' => ['name' => 'page', 'type' => 'int', 'require' => true, 'desc' => "当前页数"],
            ],
            'today' => [
                'page' => ['name' => 'page', 'type' => 'int', 'require' => true, 'desc' => "当前页数"],
            ],
        ];
    }

    /**
     * 累计签到
     * @return int uid 用户id
     * @return int days 累计签到
     * @return int constant 累计签到
     * @return int up 排名变化
     * @return int level 排名
     * @return int todayrank 今天是否签到,0表示未签到/其他表示排名
     */
    public function total()
    {
//        累计签到根据day字段排序大小

        $active = new Domain_Checkin();
        $field = 'uid,days,time,constant,up,level,todayrank';
        $pageSize = 10;
        $limit = [
            'start' => ($this->page - 1) * $pageSize,
            'end' => $pageSize
        ];
        switch ($this->type) {
            case 'total':
                $order = 'days DESC';
                return $active->total($field, $order, $limit);
                break;
            case 'succession':
                $order = 'constant DESC';
                return $active->total($field, $order, $limit);
                break;
            case 'today':
//                $time = date('Y-m-d 00:00:00');
//                $today = strtotime($time);//当前年月日
//                $arg = [
//                    'todayrank >' => 0
//                ];
                $order = 'todayrank ASC';
                $limit = [
                    'start' => ($this->page - 1) * $pageSize,
                    'end' => $pageSize
                ];
                return $active->total($field, $order, $limit);
                break;
            default:
                return '参数错误';
        }
    }

    /**
     * 今日签到
     * @return mixed
     */
    public function today()
    {
//        今日签到根据time字段截止到当前，从高到低
        $today = strtotime(date('Y-m-d 00:00:00'));//当前年月日
        $active = new Domain_Checkin();
        $field = 'uid,days,time,constant,up,level,todayrank';
        $arg = [
            'time >' => $today
        ];
        $order = 'time ASC';
        $limit = [
            'start' => ($this->page - 1) * 10,
            'end' => 10
        ];
        return $active->today($field, $arg, $order, $limit);


    }


}






























