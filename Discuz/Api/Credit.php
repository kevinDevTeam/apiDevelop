<?php

/**
 * 积分管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:02
 */
class Api_Credit extends PhalApi_Api
{
    public function getRules()
    {
        return [
            'edit' => [
                'uid' => ['name' => 'uid', 'type' => 'int','require' => true, 'errorCode' => -1101, 'desc' => '用户id'],
                'count' => ['name' => 'count', 'type' => 'int','require' => true, 'errorCode' => -1101, 'desc' => '计算后的价格'],
            ],
        ];
    }

    /**
     * 积分管理
     * @return mixed
     */
    public function edit()
    {
        $credit = new Domain_Credit();

        $arg =[
            'uid' => $this->uid,
        ];

        $data = [
            'extcredits3' => $this->count
        ];

        return $credit->editByField($arg,$data);
    }


}