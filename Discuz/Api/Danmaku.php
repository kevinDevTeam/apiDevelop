<?php

/**
 * 认证管理
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:02
 */
class Api_Danmaku extends PhalApi_Api
{
    public function getRules()
    {
        return [
            'add' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'errorCode' => -1101, 'desc' => '用户id'],
                'username' => ['name' => 'username', 'type' => 'string', 'require' => true, 'errorCode' => -1102, 'desc' => '用户名'],
                'channelId' => ['name' => 'channelId', 'type' => 'string', 'require' => true, 'errorCode' => -1103, 'desc' => '直播id'],
                'content' => ['name' => 'content', 'type' => 'string', 'require' => true, 'errorCode' => -1104, 'desc' => '内容'],
                'gift' => ['name' => 'gift', 'type' => 'array', 'require' => false, 'format' => 'json', 'desc' => '']
            ],
            'contentList' => [
//                'id' => ['name' => 'id', 'type' => 'int', 'min' => 1, 'require' => true, 'errorCode' => -1101, 'desc' => '最新的评论id'],
                'channelId' => ['name' => 'channelId', 'type' => 'string', 'require' => true, 'errorCode' => -1103, 'desc' => '直播id'],
            ],
            'getNewContentList' => [
                'id' => ['name' => 'id', 'type' => 'int', 'min' => 1, 'require' => true, 'errorCode' => -1101, 'desc' => '最新的评论id'],
                'channelId' => ['name' => 'channelId', 'type' => 'string', 'require' => true, 'errorCode' => -1103, 'desc' => '直播id'],
            ],

        ];
    }

    /**
     * 添加评论
     * @desc 添加评论
     * @return int id uid
     * @return string username 用户名
     * @return string channelId 直播间id
     * @return string content 内容
     * @return string date 评论时间
     * @return string id 评论id
     */
    public function add()
    {
        $danmaku = new Domain_Danmaku();

        if ($this->gift) {
            $data = [
                'uid' => $this->uid,
                'username' => $this->username,
                'channelId' => $this->channelId,
                'content' => $this->content,
                'imageSrc' => $this->gift['img'],
                'number' => $this->gift['number'],
                'date' => date('Y-m-d H:i:s')
            ];
        }else{
            $data = [
                'uid' => $this->uid,
                'username' => $this->username,
                'channelId' => $this->channelId,
                'content' => $this->content,
                'date' => date('Y-m-d H:i:s')
            ];
        }

        $rs = $danmaku->add($data);
        $rs['count'] = $danmaku->countNumber(['channelId'=>$this->channelId],'content');
        return $rs;
    }

    public function contentList()
    {
        $danmaku = new Domain_Danmaku();

        $arg = [
            'channelId' => $this->channelId,
        ];

//        return

        $rs = $danmaku->getDataByField('*',$arg);
        foreach($rs as $k=>$v){
            $rs[$k]['count'] = $k+1;
        }


        return $rs;
    }

    public function getNewContentList()
    {
        $danmaku = new Domain_Danmaku();

        $arg = [
            'id' => $this->id,
            'channelId' => $this->channelId,
        ];

        return $danmaku->getNewContentList($arg);
    }


}