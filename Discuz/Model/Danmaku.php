<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:06
 */

class Model_Danmaku extends PhalApi_Model_NotORM {

    //获取商家信息
    public function add($data){
        return $this->getORM()
            ->insert($data);
    }

    public function getDataByField($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchAll();
//        $sql = "SELECT * FROM pre_comment WHERE (channelId = :channelId)";
//
//        $param = [
//            ':channelId' =>$arg['channelId']
//        ];
//
//        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    public function getNewContentList($arg){
        $sql = "SELECT * FROM pre_comment WHERE id > :id AND (channelId = :channelId)";

        $param = [
            ':id' =>$arg['id'],
            ':channelId' =>$arg['channelId']
        ];

        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    public function countNumber($arg,$field){
        return $this->getORM()
            ->where($arg)
            ->count($field);
//        $sql = "SELECT count('content') FROM pre_comment WHERE (channelId = :channelId)";
//
//        $param = [
//            ':channelId' =>$arg['channelId']
//        ];
//
//        return DI()->notorm->multi_query->queryAll($sql, $param);
    }

    protected function getTableName($id) {
        return 'pre_comment';
    }

}
