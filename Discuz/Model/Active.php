<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:06
 */
class Model_Active extends PhalApi_Model_NotORM
{
    //添加活动
    public function add($data)
    {
        return $this->getORM()
            ->insert($data);
    }

    //通用查询多条方法
    public function getBaseInfo($field,$arg,$limit,$offset)
    {
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->limit($limit,$offset)
            ->fetchAll();
    }

    public function countNumber(){
        return $this->getORM()
            ->where('isHot',1)
            ->count('id');
    }

    //通用查询多条方法
    public function getHotInfo($field,$arg)
    {
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->limit(0,4)
            ->fetchAll();
    }

    public function getFiledByData($field,$arg){
        return $this->getORM()
            ->select($field)
            ->where($arg)
            ->fetchOne();
    }



    protected function getTableName($id)
    {
        return 'pre_zhibo';
    }

}
