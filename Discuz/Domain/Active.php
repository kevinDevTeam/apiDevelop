<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/10/8
 * Time: 9:06
 */
class Domain_Active {

    //插入评论资料
    public function add($data){
        $model = new Model_Active();

        return $model->add($data);
    }

    //通用查询多条方法
    public function getBaseInfo($field,$arg,$limit,$offset){
        $model = new Model_Active();

        return $model->getBaseInfo($field,$arg,$limit,$offset);
    }

    public function countNumber(){
        $model = new Model_Active();

        return $model->countNumber();
    }

    public function getHotInfo($field,$arg){
        $model = new Model_Active();

        return $model->getHotInfo($field,$arg);
    }


    public function getFiledByData($field,$arg)
    {
        $model = new Model_Active();

        return $model->getFiledByData($field,$arg);

    }


}