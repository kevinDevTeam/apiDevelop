<?php 
/*
*
*万象优图
*/
class Vientiane_Lite{

  //接口API URL前缀
  const API_URL_PREFIX = 'http://service.image.myqcloud.com/ocr/idcard';
    //开发者的项目ID
    private $appid;
    //图片空间名称bucket
    private $bucket;
    //项目的Secret ID
    private $secret_id;

    private $secret_key;
    
    private $expired;
    
    private $userid; 

    public function __construct() {
        $this->appid="1251022106";
        $this->bucket="bikeshop";
        $this->secret_id='AKIDDgj9kLcimSD3phrTYWT3Czik5DDLqvYY';
        $this->secret_key='rIl8RvRdUXAmIZyKtSET2eLMFSNnbAfH';
        $this->expired=time() + 2592000;
        $this->userid= "0";
        $this->fileid = "tencentyunSignTest";
    }
    /*
    *OCR-身份证识别
    */
     public function Obtain($url){
       $data=array(
          'a'=>$this->appid,
          'b'=>$this->bucket,
          'k'=>$this->secret_id,
          'e'=>$this->expired,
          't'=>time(),
          'r'=>rand(),
          'u'=>$this->userid,
          'f'=>'',
       	);     
      //生成签名
      $sign=$this->MakeSign($data);
         $param=array(
          "appid"    =>$this->appid,
          "bucket"   =>$this->bucket,
          "card_type"=>0,
          "url_list"=>$url,
       	);
         $xml=json_encode($param);
         //var_dump($sign);die;
     $result=$this->postXmlCurl($xml, self::API_URL_PREFIX.'?sign='.$sign);

      return json_decode($result);  
     }

  
    /**
   * 将参数拼接为url: key=value&key=value
   * @param $params
   * @return  string
   */
  public function ToUrlParams( $params ){
    $string = '';
    if( !empty($params) ){
      $array = array();
      foreach( $params as $key => $value ){
        $array[] = $key.'='.$value;
      }
      $string = implode("&",$array);
    }
    return $string;
  } 



  /**
   * 生成签名
   *  @return 签名
   */
  public function MakeSign( $params ){
    //签名步骤一：按字典序排序数组参数
   // ksort($params);
    $string = $this->ToUrlParams($params);
    //签名步骤二：在string后加入KEY
    $string =hash_hmac('SHA1', $string, $this->secret_key, true). $string;
    //print_r($string);
    //签名步骤三：base64加密
    $string = base64_encode($string);
    //签名步骤四：生成签名
    $result = $string;
    
    return $result;

  }



   /**
   * 以post方式提交xml到对应的接口url
   * 
   * @param string $xml  需要post的xml数据
   * @param string $url  url
   * @param bool $useCert 是否需要证书，默认不需要
   * @param int $second   url执行超时时间，默认30s
   * @throws WxPayException
   */
  private function postXmlCurl($xml, $url, $useCert = false, $second = 30){   
    $ch = curl_init();
    //设置超时
    curl_setopt($ch, CURLOPT_TIMEOUT, $second);
    
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
    //设置header
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    //要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  
    //post提交方式
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    //运行curl
    $data = curl_exec($ch);
    //返回结果
    
    if($data){
      curl_close($ch);
      return $data;
    } else { 
      $error = curl_errno($ch);
      curl_close($ch);
      return false;
    }
  }


  }


  ?>