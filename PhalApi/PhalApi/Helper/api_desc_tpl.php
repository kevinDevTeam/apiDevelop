<?php
echo <<<EOT
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{$service} - 在线接口文档</title>
    <meta name="robots" content="none"/>
    <link  href="//cdn.bootcss.com/semantic-ui/2.2.2/semantic.min.css" rel="stylesheet">


    <link rel="stylesheet" href="//cdn.bootcss.com/codemirror/5.12.0/codemirror.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/codemirror/5.12.0/theme/monokai.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/codemirror/5.12.0/addon/fold/foldgutter.min.css">
    <script src="//cdn.bootcss.com/jquery/2.2.1/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/codemirror/5.12.0/codemirror.js"></script>
    <script src="//cdn.bootcss.com/codemirror/5.12.0/addon/fold/foldcode.min.js"></script>
    <script src="//cdn.bootcss.com/codemirror/5.12.0/addon/fold/foldgutter.min.js"></script>
    <script src="//cdn.bootcss.com/codemirror/5.12.0/mode/javascript/javascript.min.js"></script>
    <style>
    .CodeMirror pre{word-break: break-all;}
    </style>
</head>

<body>

<br />

    <div class="ui text container" style="max-width: none !important;margin-bottom:1em">
        <div class="ui floating message">

EOT;

echo "<h2 class='ui header'>接口：$service</h2><br/> <span class='ui teal tag label'>$description</span>";

echo <<<EOT
            <div class="ui raised segment">
                <span class="ui red ribbon label">接口说明</span>
                <div class="ui message">
                    <p>{$descComment}</p>
                </div>
            </div>
            <h3>接口参数</h3>
            <table class="ui red celled striped table" >
                <thead>
                    <tr><th>参数名字</th><th>类型</th><th>是否必须</th><th>默认值</th><th>其他</th><th>说明</th></tr>
                </thead>
                <tbody>
EOT;

foreach ($rules as $key => $rule) {
    $name = $rule['name'];
    if (!isset($rule['type'])) {
        $rule['type'] = 'string';
    }
    $type = isset($typeMaps[$rule['type']]) ? $typeMaps[$rule['type']] : $rule['type'];
    $require = isset($rule['require']) && $rule['require'] ? '<font color="red">必须</font>' : '可选';
    $default = isset($rule['default']) ? $rule['default'] : '';
    if ($default === NULL) {
        $default = 'NULL';
    } else if (is_array($default)) {
        $default = json_encode($default);
    } else if (!is_string($default)) {
        $default = var_export($default, true);
    }

    $other = '';
    if (isset($rule['min'])) {
        $other .= ' 最小：' . $rule['min'];
    }
    if (isset($rule['max'])) {
        $other .= ' 最大：' . $rule['max'];
    }
    if (isset($rule['range'])) {
        $other .= ' 范围：' . implode('/', $rule['range']);
    }
    $desc = isset($rule['desc']) ? trim($rule['desc']) : '';

    echo "<tr><td>$name</td><td>$type</td><td>$require</td><td>$default</td><td>$other</td><td>$desc</td></tr>\n";
}

echo <<<EOT
                </tbody>
            </table>
EOT;
echo <<<EOT
<script>
EOT;
if($rules) {
    echo "var rules=".json_encode($rules);
}else{
    echo "var rules=false";
}
echo <<<EOT
</script>
EOT;

if(!empty($returns)){


    echo <<<EOT
            <h3>返回结果</h3>
            <table class="ui green celled striped table" >
                <thead>
                    <tr><th>返回字段</th><th>类型</th><th>说明</th></tr>
                </thead>
                <tbody>
EOT;

    foreach ($returns as $item) {
        $name = $item[1];
        $type = isset($typeMaps[$item[0]]) ? $typeMaps[$item[0]] : $item[0];
        $detail = $item[2];

        echo "<tr><td>$name</td><td>$type</td><td>$detail</td></tr>";
    }
    echo <<<EOT
            </tbody>
        </table>
EOT;
}

if(!empty($throws)){


    echo <<<EOT
            <h3>错误码</h3>
            <table class="ui green celled striped table" >
                <thead>
                    <tr><th width="200">错误码</th><th>说明</th></tr>
                </thead>
                <tbody>
EOT;

    foreach ($throws as $item) {
        echo "<tr><td>$item[0]</td><td>$item[1]</td></tr>";
    }

    echo <<<EOT
            </tbody>
        </table>
EOT;
}

$version = PHALAPI_VERSION;

echo <<<EOT

        <div class="ui blue message">
          <strong>温馨提示：</strong> 此接口参数列表根据后台代码自动生成，可将 ?service= 改成您需要查询的接口/服务
        </div>
        <!--API调试-->
<div class="ui raised segment">
                <span class="ui red ribbon label">API调试</span>
                <div class="ui form">


        <form id="ApiTest" style="margin-top: 1em;">
           <div class="fields">
                        <div class="three wide field">
                            <label>Method</label>
                            <select class="ui fluid search dropdown" id="js-req-method">
                                <option value="GET">GET</option>
                                <option value="POST" selected="">POST</option>
                                <option value="PUT">PUT</option>
                                <option value="PATCH">PATCH</option>
                                <option value="DELETE">DELETE</option>
                                <option value="COPY">COPY</option>
                                <option value="HEAD">HEAD</option>
                                <option value="OPTIONS">OPTIONS</option>
                                <option value="LINK">LINK</option>
                                <option value="UNLINK">UNLINK</option>
                                <option value="PURGE">PURGE</option>
                            </select>
                        </div>
                        <div class="ten wide field">
                            <label>API URL</label>
EOT;


echo  '<input type="text" class="form-control" id="apiServiceUrl" readonly  value="http://'.str_replace("/Public","",str_replace("checkApiParams.php","",$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"])).'">';
//echo  '<input type="text" class="form-control" id="apiServiceUrl" readonly  value="http://'.str_replace("/Public","",str_replace("checkApiParams.php","",$_SERVER["SERVER_NAME"].':81'.$_SERVER["REQUEST_URI"])).'">';
echo <<<EOT
                        </div>
                        <div class="two wide field">
                            <label> &nbsp;</label>
                            <button   type="submit" class="ui inverted green button">发送 </button>

                        </div>
                    </div>


             <table class="ui orange celled striped table selectable"  id="RequestTable" >
                            <thead>
                            <tr class="info">
                                <th style="width: 300px">参数</th>
                                <th>值</th>
                                <th class="right aligned collapsing">  <span class="ui inverted purple button" id="addParameter">添加</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>


 <table class="ui celled table olive striped" style="display: none"  id="js-res">
                        <thead>
                        <tr class="info">
                            <th  style="width: 300px">Response Header</th>
                            <th>Response Body</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <tr>
                            <td style="width: 300px">
                                <textarea id="js-res-header" class="form-textarea"></textarea>
                            </td>
                            <td>
                                <textarea id="js-res-content" class="form-textarea"></textarea>
                            </td>
                        </tr>
                        </tr>
                        </tbody>
                    </table>

        </form>
          </div>
            </div>
       <script type="text/javascript" language="javascript">
            var JQ=jQuery.noConflict();JQ(function(){var a=' <tr><td> <input type="text" class="form-control"  maxlength="100"> </td><td><input type="text" class="form-control" maxlength="5000"> </td> <td><span  class="ui inverted red button btn-del">X</span> </td> </tr>';rules&&(JQ.each(rules,function(b,c){c.require&&JQ("#RequestTable tbody").append(a.replace('maxlength="100"','maxlength="100" value="'+c.name+'"').replace('maxlength="5000"','maxlength="5000" placeholder="'+c.desc+'"'))})),JQ("#addParameter").on("click",function(){JQ("#RequestTable tbody").append(a)}),JQ("#RequestTable").on("click",".btn-del",function(){JQ(this).parents("tr").remove()});var b=CodeMirror.fromTextArea(JQ("#js-res-header")[0],{value:"",lineNumbers:!1,mode:"javascript",theme:"monokai",indentUnit:4,lineWrapping:!1,styleActiveLine:!0,matchBrackets:!0,readOnly:!0}),c=CodeMirror.fromTextArea(JQ("#js-res-content")[0],{value:"",lineNumbers:!0,theme:"monokai",indentUnit:4,lineWrapping:!0,styleActiveLine:!0,matchBrackets:!0,autoCloseBrackets:!0,foldGutter:!0,gutters:["CodeMirror-linenumbers","CodeMirror-foldgutter"],readOnly:!0});JQ("#ApiTest").on("submit",function(){var a="",d=!0,e=JQ("#apiServiceUrl").val();return e?(JQ("#RequestTable input").each(function(b){if(!d)return!1;var c=JQ(this).val();c?a+=b%2==0?c:"="+c+"&":(JQ(this).focus(),alert((b%2==0?"参数":"参数值")+"必填"),d=!1)}),d?(console.log(a),JQ.ajax({url:e,method:JQ("#js-req-method").val(),data:a,dateType:"json",success:function(a,d,e){JQ("#js-res").show(),b.setValue(decodeURIComponent(e.getAllResponseHeaders())),c.setValue(vkbeautify.json(a,4)),JQ("html,body").animate({scrollTop:JQ("#js-res").offset().top},800)},error:function(a){JQ("#js-res").show(),b.setValue(decodeURIComponent("status:"+a.status)),c.setValue(vkbeautify.json(JSON.stringify(a.statusCode()),4)),JQ("html,body").animate({scrollTop:JQ("#js-res").offset().top},800)}}),!1):!1):!1})});
        </script>
        <!--API调试-->
        <p>&copy; Powered  By <a href="http://www.phalapi.net/" target="_blank">PhalApi {$version}</a> <p>
        </div>
    </div>
</body>
</html>
EOT;


